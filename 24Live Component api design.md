
https://www.sofascore.com/football//2018-01-20/json?_=153242479

# Global

### Button
* icon: `string` | `null`
* title: `string`
* iconProps: `Object`
* titleProps: `Object`

## ButtonIcon
* icon: `string`
* iconProps: `Object`
* titleProps: `Object`

### IconInfo
Params:     
* icon: `string` | `React.Component` | `(props) => React.Component`
* title: `string` | `React.Component` | `(props) => React.Component`
* direction: `ltr` | `rtl`
* theme: `dark` | `light`
* iconProps: `Object`
* titleProps: `Object`

```jsx
    <IconInfo
        icon="place"
        title="World Cup 2018"
        iconProps={{
            color: '#ffffff',
            size: 24,
        }}
        titleProps={{
            color: '#ff0000',
        }}
    />

    <IconInfo
        icon={<Image source={{ uri: '<url>'}} />}
        title="World Cup 2018"
    />

    <IconInfo
        icon={(props) =>
            <Image {...props} source={{ uri: '<url>'}} />
        }
        title="World Cup 2018"
    />
```


# Match

1. Match Item
    Building Components
    - Team
        Params:
        * name: `string`
        * flag: `string`
        * position: `home` | `away`
        * theme: `dark` | `light`
    - Result
        * time: `string`
        * result: `{ home: 2, away: 0 }` | `null`
        * minute: `number` | `string` (`FT` | `HT`)
        * started: `boolean`
    Main Component
    - MatchItem
        * match: `Object`
        Example: 
        ```json
            {
                "homeTeam": {},
                "awayTeam": {},
                "result": {
                    "home": 2,
                    "away": 0
                }
                // ...
            }
        ```
2. Match List

3. Single Match
    Building Components
    - MatchSummary
        - Team
            Params:
            * name: `string`
            * flag: `string`
            * theme: `dark` | `light`
        - MatchStadium
            Params:
            * stadium: `Object`
        - MatchResult
            Params:
            * result: `{ home: 2, away: 0 }` | `null`
            * started: `boolean`
        - MatchMinute
            * status: `not-started` | `started` | `ended` | `suspended`
            * time: `string`
            * minute: `number` | `string` (`FT` | `HT`)

    - MatchEvents
        - Event
            * position: `home` | `away` | `middle`
            * type: `string` | `number`
            * minute: `number`
            * player: `string` | `null`
            * player2: `string` | `null`
            * match: `Object`

# Competition

1. Competition Item
    Building Components
    - Competition Name
        Params:
        * name: `string`
        * flag: `string`
        * theme: `dark` | `light`
    - StarButton
        - isActive: `boolean`
    - MatchNumberBadge
        - matchNumber: `number`


# News
Building Components
1. NewsItem
    * image: `string`
    * title: `string`
    * date: `string`
    * summary: `string`
    * layout: `card` | `list`

2. NewsCardItem
    * image: `string`
    * title: `string`
    * date: `string`
    * summary: `string`

2. NewsListItem
    * image: `string`
    * title: `string`
    * date: `string`
    * summary: `string`
    