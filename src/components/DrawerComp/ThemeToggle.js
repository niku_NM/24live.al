// @flow
import React from 'react';
import { View, Switch } from 'react-native';
import Text from '../Global/Text';
import TouchableItem from '../Global/Button/TouchableItem';
import Icon from '../Global/Icon';
import { withTheme } from '../Global/Providers/ThemeProvider';
import Theme from '../Global/Theme';

type Props = {
    theme: 'light' | 'dark';
}

const styles = {
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 16,
        height: 56
    },
    text: {
        paddingLeft: 32,
        fontWeight: 'bold',
    }
};


class ThemeToggle extends React.Component<Props, {}> {
    state = { darkThemeComponent: true };
    
    render() {
    const { theme } = this.props;
    const color = Theme.getTextColor(theme);
    return (
        <TouchableItem
            onPress={() => {
                this.props.toggleTheme();
            }}
            {...this.props}
        >
           <View style={styles.container}>
            <Icon
                name={theme === 'light' ? 'brightness-2' : 'brightness-5'}
                color={color}
                size={24}
            />
            <Text
                style={[styles.text, { color }]}
            >
                {/* { theme === 'light' ? 'Dark Theme' : 'Light Theme'} */}
                Light Theme
            </Text>
            <View style={{ alignItems: 'flex-end', flex: 1 }}>
            <Switch 
                value={theme === 'light'} 
                onValueChange={() => { this.props.toggleTheme(); }} 
            />
            </View>
        </View>
        </TouchableItem>
    );
}
}

export default withTheme(ThemeToggle);

