import React from 'react';
import { View } from 'react-native';

import Text from '../Global/Text';
import { withTheme } from '../Global/Providers/ThemeProvider';
import Theme from '../Global/Theme';
import TouchableItem from '../Global/Button/TouchableItem';
import Icon from '../Global/Icon';

const styles = {
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 16,
        height: 56
    },
    text: {
        paddingLeft: 32,
        fontWeight: 'bold',
    }
};

const CustomDrawerLabel = (props) => {
    const color = Theme.getTextColor(props.theme);
    return (
        <TouchableItem 
            onPress={props.onPress}
            style={styles.container}
        >
            <Icon
                name={props.iconName}
                color={color}
                size={24}
            />
            <Text style={[styles.text, { color }]}>
                {props.title}
            </Text>
        </TouchableItem>
    );
};

export default withTheme(CustomDrawerLabel);
