// @flow
import React from 'react';
import { 
    View, 
    StyleSheet, 
    ScrollView,
    Share,
 } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import _ from 'lodash';
import HTMLView from 'react-native-htmlview';
import FastImage from 'react-native-fast-image';

import Text from '../Global/Text';
import { withTheme } from '../Global/Providers/ThemeProvider';
import Theme from '../Global/Theme';
import { ButtonIcon } from '../Global/Button/ButtonIcon';

type Props = {
    imageUrl: string;
    title: string;
    category: string;
    date: number | string;
    content: string;
    fontSize: number;
    theme: 'dark' | 'light';
}

const styles = {
    title: {
        fontWeight: 'bold',
        fontSize: 16,
        width: '80%'
    },
    categoryContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 4,
    },
    imgStyle: {
        height: 220,
        width: '100%',
        // resizeMode: 'cover'
    },
};

// const SCREEN_WIDTH = Dimensions.get('window').width;

const Category = withTheme((props: Props) => {
    const { category, date, ...rest } = props;
    const color = Theme.getPrimaryColor(rest.theme);
    const dateColor = Theme.getTextColor(rest.theme, 1);
    return (
        <View
            style={[
                styles.categoryContainer,
                { backgroundColor: Theme.getBgColor(rest.theme, 2) }
            ]}
        >
            <Text
                numberOfLines={1} style={[styles.title, { color }]}
            >
                {category}
            </Text>
            <Text style={{ fontSize: 11, color: dateColor, }}>
                {date}
            </Text>
        </View>
    );
});

const NewsTitle = withTheme((props: Props) => {
    const { fontSize, theme, title } = props;
    const backgroundColor = Theme.getTextColor(theme, 2);
    const color = Theme.getTextColor(theme);

    return (
        <View style={{ marginBottom: 6 }}>
            <Text style={{ fontSize, margin: 8, color }} >{title}</Text>
            <View style={{ height: 1, backgroundColor, marginHorizontal: 10 }} />
        </View>
    );
});

const Content = withTheme((props: Props) => {
    const { content, theme, fontSize } = props;
    const color = Theme.getTextColor(theme);
    const htmlStyle = StyleSheet.create({
        h4: {
          fontSize,
          color,
        },
        p: {
            fontSize,
            color,
        },
        a: {
            fontSize,
            color
        }
      });

    return (
        <View style={{ margin: 10 }}>
            <HTMLView
                value={content}
                stylesheet={htmlStyle}
                onLinkPress={(url) => console.log('clicked link: ', url)}
            />
        </View>
    );
});


class OpenedNews extends React.Component {
    state = { 
        smallText: true,
        loading: false,
        categories: '',
        content: '',
        date: '',
        _embedded: [],
        title: '',
        url: ''
    };

    componentDidMount() {
        this.setState({ loading: true });
        this.props.navigation.setParams({
            toggleLayout: this.toggleText,
            smallText: this.state.smallText,
            shareNews: this.shareNews,
        });
        const data = this.props.navigation.getParam('data', {});
        
        this.setState({ 
           categories: data.subtitle,
           content: data.content.rendered,
           date: data.date,
           title: data.title.rendered,
           _embedded: data._embedded,
           url: data.link,
        });
    }

    getImgUrl(_embedded) {
        return _.get(
            _embedded,
            'wp:featuredmedia[0].media_details.sizes.full.source_url',
            'https://dvynr1wh82531.cloudfront.net/sites/default/files/default_images/noImg_2.jpg'
        );
    }

    toggleText = () => {
        this.setState((prevState) => ({
            smallText: !prevState.smallText,
        }), () => {
            this.props.navigation.setParams({
                smallText: this.state.smallText,
            });
        });
    }

    shareNews = () => {
        Share.share({
            message: this.state.url,
            url: this.state.url,
            title: 'Shpernda lajmin'
          }, {
            // Android only:
            dialogTitle: 'Shpernda lajmin'
          }); 
    }

    render() {
        const backgroundColor = Theme.getBgColor(this.props.theme);
        // const grey = Theme.getTextColor(this.props.theme, 1);
        const { content, date, title, categories, _embedded } = this.state;

        return (
            <ScrollView style={{ backgroundColor }}>
                <FastImage 
                    source={{ uri: this.getImgUrl(_embedded) }} 
                    style={styles.imgStyle}
                    resizeMode={FastImage.resizeMode.cover}
                />
                <Category category={categories} date={moment(date).format('DD-MM-YYYY')} />
                <NewsTitle fontSize={this.state.smallText ? 16 : 18} title={title} />
                <Content fontSize={this.state.smallText ? 14 : 16} content={content} />
            </ScrollView>
        );
    }
}

const OpenedNewsWithTheme = withTheme(OpenedNews);

OpenedNewsWithTheme.navigationOptions = ({ navigation }) => {
    const toggleLayout = navigation.getParam('toggleLayout', () => { });
    const shareNews = navigation.getParam('shareNews', () => { });

    return {
        headerTransparent: true,
        headerTintColor: 'white',
        headerBackground: (
            <View
                style={[
                    StyleSheet.absoluteFill,
                ]}
            >
                <LinearGradient
                    colors={['rgba(0,0,0,1)', 'rgba(0,0,0,0)']}
                    locations={[0, 1]}
                    style={[{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                    }]}
                />
            </View>
        ),
        headerRight: (
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <ButtonIcon icon="text-fields" flat onPress={toggleLayout} />
                <ButtonIcon icon="share" flat onPress={shareNews} />
            </View>
        ),
    };
};

export default OpenedNewsWithTheme;
