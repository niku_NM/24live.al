// @flow

import React from 'react';
import { View, } from 'react-native';
import FastImage from 'react-native-fast-image';

import Text from '../Global/Text';
import Theme from '../Global/Theme';
import { withTheme } from '../Global/Providers/ThemeProvider';
import TouchableItem from '../Global/Button/TouchableItem';
import CompetitionListWraper from '../Competition/CompetitionListWraper';

const styles = {
    img: {
       width: '36%',
       height: 84,
    //    resizeMode: 'cover',
       borderRadius: 10
    },
    container: {
        height: 84,
        justifyContent: 'flex-start',
        marginHorizontal: 6,
        flex: 1
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: 'grey',
        borderBottomWidth: 0.5
    },
    title: {
        flex: 1,
        fontWeight: '200',
        fontSize: 17,
    },
    date: {
        fontSize: 11,
        paddingLeft: 6,
    },
    contentText: {
        fontWeight: '400',
        fontSize: 16,
    }
};

type Props = {
    imageUrl: string;
    title: string;
    date: number | string;
    subtitle: string;
    theme: 'dark' | 'light';
    onPress: Function;
}

class ListItem extends React.Component<Props, {}> {
    static defaultProps = {
        theme: 'dark'
    }

    render() {
        const { imageUrl, title, date, subtitle, onPress, ...rest } = this.props;
        const color = Theme.getPrimaryColor(rest.theme);
        const dateColor = Theme.getTextColor(rest.theme, 1);
        const contentColor = Theme.getTextColor(rest.theme);
        const backgroundColor = Theme.getBgColor(rest.theme, 1);
        return (
            <CompetitionListWraper>
            <TouchableItem onPress={onPress}>
                <View style={{ flexDirection: 'row', backgroundColor }}>
                    <FastImage
                        source={{ uri: imageUrl }}
                        style={styles.img}
                        resizeMode={FastImage.resizeMode.cover}
                    />
                    <View style={styles.container}>
                        <View style={styles.titleContainer}>
                            <Text 
                                numberOfLines={1} 
                                style={[styles.title, { color }]}
                            >
                             {subtitle}
                             </Text>
                            <Text style={[styles.date, { color: dateColor }]}>
                                {date}
                            </Text>
                        </View>
                        <View>
                            <Text 
                                numberOfLines={3} 
                                style={[styles.contentText, { color: contentColor }]}
                            >
                             {title}
                            </Text>
                        </View>
                    </View>
                </View>
            </TouchableItem>
            </CompetitionListWraper>
        );
    }
}

export default withTheme(ListItem);

