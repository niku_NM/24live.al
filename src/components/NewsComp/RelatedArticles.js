// @flow
import React from 'react';
import { View, Image } from 'react-native';

import Text from '../Global/Text';
import { withTheme } from '../Global/Providers/ThemeProvider';
import Theme from '../Global/Theme';
import Title from '../Global/Title';


type Props = {
    imgUrl: string;
    title: string;
    date: string | number;
    theme: 'dark' | 'light';
}

const IMG_URL = 'https://imga.androidappsapk.co/poster/f/3/5/com.sports4kwallpapers_4.png';

class RelatedArticles extends React.Component<Props, {}> {
    render() {
        const color = Theme.getTextColor(this.props.theme);
        const grey = Theme.getTextColor(this.props.theme, 2);
        return (
            <View style={{ flexDirection: 'row', marginVertical: 8, marginHorizontal: 16, }}>
                <Image source={{ uri: IMG_URL }} style={{ width: 130, height: 90, borderRadius: 10 }} />
                <View style={{ flex: 1, paddingLeft: 6, marginVertical: 2 }}>
                    <Text numberOfLines={3} style={{ fontSize: 16, color, lineHeight: 22 }}>{this.props.title}</Text>
                    <View style={{ justifyContent: 'flex-end', flex: 1 }}>
                    <Text style={{ color: grey }}>{this.props.date}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

export default withTheme(RelatedArticles);
