// @flow

import React from 'react';
import { View, Dimensions } from 'react-native';
import FastImage from 'react-native-fast-image';

import Text from '../Global/Text';
import Theme from '../Global/Theme';
import { withTheme } from '../Global/Providers/ThemeProvider';
import TouchableItem from '../Global/Button/TouchableItem';


const SCREEN_WIDTH = Dimensions.get('window').width;

const styles = {
    container: {
        margin: 12,
        borderRadius: 6,
        overflow: 'hidden',
        elevation: 10,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
        width: '80%'
    },
    titleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 4,
    },
    content: {
        justifyContent: 'center',
        padding: 6,
    }
};

type Props = {
    imageUrl: string;
    subtitle: string;
    date: number | string;
    title: string;
    theme: 'dark' | 'light';
    onPress: Function;
}

class CardItem extends React.Component<Props, {}> {
    static defaultProps = {
        theme: 'dark'
    }

    render() {
        const { imageUrl, subtitle, date, title, onPress, ...rest } = this.props;
        const color = Theme.getPrimaryColor(rest.theme);
        const dateColor = Theme.getTextColor(rest.theme, 1);
        const contentColor = Theme.getTextColor(rest.theme);
        const backgroundColor = Theme.getBgColor(rest.theme, 1);
        return (
            <TouchableItem onPress={onPress}>
                <View style={styles.container}>
                    <FastImage
                        source={{ uri: imageUrl }}
                        style={{ width: SCREEN_WIDTH - 20, height: 190, }}
                        resizeMode={FastImage.resizeMode.cover}
                    />
                    <View 
                     style={[
                        styles.titleContainer, 
                        { backgroundColor: Theme.getBgColor(rest.theme, 2) }
                        ]}
                    >
                         <Text
                            numberOfLines={1} style={[styles.title, { color }]}
                         >
                            {subtitle}
                        </Text>
                        <Text style={{ fontSize: 11, color: dateColor, }}>
                            {date}
                        </Text>
                    </View>
                    <View style={[styles.content, { backgroundColor }]}>
                        <Text
                            numberOfLines={2} 
                            style={{ fontSize: 17, fontWeight: '300', color: contentColor }}
                        >
                            {title}
                        </Text>
                    </View>
                </View>
            </TouchableItem>
        );
    }
}

export default withTheme(CardItem);
