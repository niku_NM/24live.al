import React from 'react';
import { View, Dimensions } from 'react-native';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import Modal from 'react-native-modal';
import moment from 'moment';

import ButtonIcon from '../Global/Button/ButtonIcon';
import { withTheme } from '../Global/Providers/ThemeProvider';
import Theme from '../Global/Theme';
import { withDateUpdated } from '../Global/Providers/CalendarProvider';

LocaleConfig.locales['.alb'] = {
    monthNames: 
    ['Janar', 'Shkrurt', 'Mars', 'Prill', 'Maj', 'Qeshor',
    'Korrik', 'Gusht', 'Shtator', 'Tetor', 'Nentor', 'Dhjetor'],
    monthNamesShort: 
    ['Jan.', 'Shkr.', 'Mars', 'Pri', 'Maj', 'Qesh', 'Korr.', 'Gush', 'Shta', 'Tet', 'Nen', 'Dhje'],
    dayNames: ['Diele', 'Hene', 'Marte', 'Merkure', 'Enjte', 'Premte', 'Shtune'],
    dayNamesShort: ['Die.', 'Hen.', 'Mar.', 'Mer.', 'Enj.', 'Pre.', 'Sht.']
};

LocaleConfig.defaultLocale = '.alb';

const SCREEN_HEIGHT = Dimensions.get('window').height;

class ShowCalendar extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            modalVisible: false,
            date: moment(),
            loading: false,
        };
        this.onDayPress = this.onDayPress.bind(this);
    }

    onDayPress(day) {
        this.setState({
            modalVisible: false,
        });
        this.props.changeDate(day.dateString);
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    toggle = () => {
        this.setModalVisible(!this.state.modalVisible);
    }

    get icon() {
        if (this.state.modalVisible) {
            return 'close';
        }
        return 'event';
    }

    render() {
        return (
            <View>
                <ButtonIcon flat icon={this.icon} onPress={this.toggle} />
                <Modal
                    isVisible={this.state.modalVisible}
                    onBackdropPress={() => { this.setModalVisible(false); }}
                    onBackButtonPress={() => { this.setModalVisible(false); }}
                >
                    {/* <View style={{ flex: 1, position: 'absolute', top: 140, left: 0, right: 0, }}> */}
                    <View style={styles.containerStyle}>
                        <Calendar
                            current={this.props.currentDate}
                            onDayPress={this.onDayPress}
                            markedDates={{ [this.props.currentDate]: { selected: true, } }}
                            hideExtraDays
                            firstDay={1}
                            onPressArrowLeft={substractMonth => substractMonth()}
                            onPressArrowRight={addMonth => addMonth()}
                            style={{ 
                                borderWidth: 1, 
                                borderColor: Theme.getTextColor(this.props.theme, 1), 
                                elevation: 6 
                            }}
                            theme={{
                                calendarBackground: Theme.getBgColor(this.props.theme, 2),
                                textSectionTitleColor: '#898F98',
                                selectedDayBackgroundColor: '#09BA85',
                                todayTextColor: '#09BA85',
                                dayTextColor: Theme.getTextColor(this.props.theme, 1),
                                monthTextColor: '#09BA85',
                                arrowColor: Theme.getTextColor(this.props.theme, 1),
                            }}
                        />
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        flex: 1, 
        position: 'absolute', 
        top: SCREEN_HEIGHT / 5, 
        left: 0, 
        right: 0
    }
};

const ThemedCalendar = withDateUpdated(withTheme(ShowCalendar));

export default ThemedCalendar;

