// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';

import Text from '../../../Global/Text';
import Theme from '../../../Global/Theme';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import EventIcon from '../Events/EventIcon';
import CompetitionListWraper from '../../../Competition/CompetitionListWraper';
import Title from '../../../Global/Title';
import Divider from '../../../Global/Divider';


type Props = {
    title: string;
    htValue: string | number;
    atValue: string | number;
    theme: 'dark' | 'light';
    type: number;
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingBottom: 16,
        justifyContent: 'space-between'
    },
    text: {
        textAlign: 'center',
        fontWeight: '600'
    },
    title: {
        paddingVertical: 6,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemSeperator: {
        height: 1,
        alignSelf: 'center',
        width: '100%',
    }
});

const CartonBarItem = withTheme((props: Props) => {
    const { title, htValue, atValue, theme, type } = props;
    const titleColor = Theme.getPrimaryColor(theme);
    const color = Theme.getTextColor(theme);

    return (
        <View >
            <View style={styles.title}>
                <Text
                    style={{ color: titleColor, fontSize: 16 }}
                >
                    {title}
                </Text>
            </View>
            <View style={styles.container}>
                <Text style={{ color, width: 24, textAlign: 'center' }}>{htValue}</Text>
                <EventIcon size={24} eventType={type} theme={props.theme} />
                <Text style={{ color, width: 24, textAlign: 'center' }}>{atValue}</Text>
            </View>
        </View>
    );
});

class CartonBar extends React.Component {
    render() {
        return (
            <View>
                <Title title='Cartons' />
                <CompetitionListWraper
                    style={{
                        paddingHorizontal: 16,
                        backgroundColor: Theme.getBgColor(this.props.theme, 1)
                    }}
                >
                    <CartonBarItem
                        title='Yellow Cards'
                        htValue={6}
                        atValue={4}
                        type={6}
                    />
                    <Divider />
                    <CartonBarItem
                        title='Red Cards'
                        htValue={12}
                        atValue={1}
                        type={5}
                    />
                    <Divider />
                    <CartonBarItem
                        title='Yellow To Red Cards'
                        htValue={3}
                        atValue={16}
                        type={24}
                    />
                </CompetitionListWraper>
            </View>
        );
    }
}


export default withTheme(CartonBar);

