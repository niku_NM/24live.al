// @flow
import * as React from 'react';
import { View, StyleSheet } from 'react-native';

import Text from '../../../Global/Text';
import Theme from '../../../Global/Theme';
import { withTheme } from '../../../Global/Providers/ThemeProvider';

type Props = {
    title: string | React.Component<any> | (props: any) => React.Component<any>;
    titleProps: any;
    theme: 'light' | 'dark';
    values: [number, number];
    formatValue: (value: number) => string;
};

const Colors = {
    dark: {
        backgroundColor: 'rgba(255,255, 255, 0.2)',
        progressColor: 'rgba(255,255,255,0.5)',
        activeProgressColor: 'rgba(255,255,255,0.8)',
    },
    light: {
        backgroundColor: 'rgba(0,0, 0, 0.2)',
        progressColor: 'rgba(0,0,0,0.5)',
        activeProgressColor: 'rgba(0,0,0,0.8)',
    }
};

const Bar = ({
    percent = '50%', align = 'left', active, style, theme }: { theme: 'light' | 'dark' }) => {
    const borderTopRightRadius = align === 'left' ? 24 : 0;
    const borderBottomRightRadius = align === 'left' ? 24 : 0;
    const borderTopLeftRadius = align === 'left' ? 0 : 24;
    const borderBottomLeftRadius = align === 'left' ? 0 : 24;

    const colors = Colors[theme] || Colors.dark;

    const backgroundColor = active ? colors.activeProgressColor : colors.progressColor;

    return (
        <View
            style={[{
                flexDirection: 'row',
                justifyContent: align === 'left' ? 'flex-start' : 'flex-end',
                overflow: 'hidden',
                backgroundColor: colors.backgroundColor,
                borderTopRightRadius,
                borderBottomRightRadius,
                borderTopLeftRadius,
                borderBottomLeftRadius,
            }, style]}
        >
            <View
                style={{
                    width: percent,
                    height: 20,
                    backgroundColor,
                    borderTopRightRadius,
                    borderBottomRightRadius,
                    borderTopLeftRadius,
                    borderBottomLeftRadius,
                }}
            />
        </View>
    );
};


function getMaxValue(values = []) {
    return values.reduce((max, value, index) => {
        if (max.value > value) {
            return max;
        }
        return {
            value,
            index,
        };
    }, {
            value: -Infinity,
            index: -1,
        });
}

function getTotal(values = []) {
    const max = getMaxValue(values);
    const difference = Math.abs(values[0] - values[1]);
    return max.value + difference;
}

class InfoBar extends React.Component<Props, {}> {
    static defaultProps = {
        titleProps: {},
        theme: 'dark',
        formatValue: (value) => value
    }
    state = { total: 0 };

    componentDidMount() {
        const total = getTotal(this.props.values);
        this.setState({ total });
    }

    get firstItemWidth() {
        if (this.state.total === 0) { return 0; }

        return `${(this.props.values[0] / this.state.total) * 100}%`;
    }

    get secondItemWidth() {
        if (this.state.total === 0) { return 0; }

        return `${(this.props.values[1] / this.state.total) * 100}%`;
    }

    get activeIndex() {
        const [first, second] = this.props.values;
        if (first === second) { return -1; }

        if (first > second) {
            return 0;
        }

        return 1;
    }

    renderValue(value) {
        return (
            <View style={{ justifyContent: 'center', width: 36 }}>
                <Text style={[styles.text, { color: Theme.getTextColor(this.props.theme) }]}>
                    {this.props.formatValue(value)}
                </Text>
            </View>
        );
    }

    render() {
        const { title, titleProps, theme } = this.props;
        const backgroundColor = Theme.getBgColor(theme, 1);
        const titleColor = Theme.getPrimaryColor(theme);
        return (
            <View style={[styles.container, { backgroundColor }]}>
                <View style={{ paddingVertical: 4 }}>
                    <Text
                        {...titleProps}
                        style={{ color: titleColor, }}
                    >
                        {title}
                    </Text>
                </View>
                <View style={{ flexDirection: 'row', paddingBottom: 12 }}>
                    {this.renderValue(this.props.values[0])}
                    <View style={{ width: 6 }} />
                    <Bar
                        theme={this.props.theme}
                        style={{ flex: 1 }}
                        align="right"
                        percent={this.firstItemWidth}
                        active={this.activeIndex === 0}
                    />
                    <View style={{ width: 4 }} />
                    <Bar
                        theme={this.props.theme}
                        style={{ flex: 1 }}
                        percent={this.secondItemWidth}
                        active={this.activeIndex === 1}
                    />
                    <View style={{ width: 6 }} />
                    {this.renderValue(this.props.values[1])}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        textAlign: 'center',
        fontWeight: '600'
    },
});

export default withTheme(InfoBar);
