import React from 'react';
import { FlatList, View } from 'react-native';

import Divider from '../../../Global/Divider';
import CompetitionListWraper from '../../../Competition/CompetitionListWraper';
import InfoBar from './InfoBar';
import statisticsMap from './statisticsMap';


class StatsTab extends React.Component {
    state = {
        data: [],
    }
    
    componentDidMount() {
        this.setState({
            data: statisticsMap.map(item => {
                const home = Number(this.props.stats[item.home]);
                const away = Number(this.props.stats[item.away]);
                const { label, formated } = item;
                return {
                    home: Number.isNaN(home) ? 0 : home,
                    away: Number.isNaN(away) ? 0 : away,
                    label,
                    formated
                };
            }),
        });
    }
    renderItem = ({ item }) => {
        const { home, away, label, formated } = item;
        return (
            <React.Fragment>
                {formated ?
                    <View>
                        <InfoBar
                            values={[home, away]}
                            title={label}
                            formatValue={value => `${value}%`}
                        />
                        <Divider style={{ marginHorizontal: 16, }} />
                    </View>
                    :
                    <View>
                        <InfoBar
                            values={[home, away]}
                            title={label}
                        />
                        <Divider style={{ marginHorizontal: 16, }} />
                    </View>
                }
            </React.Fragment>
        );
    }

    render() {
        return (
            <CompetitionListWraper>
                <FlatList
                    data={this.state.data}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.label}
                    ItemSeparatorComponent={() => <Divider />}
                />
            </CompetitionListWraper>
        );
    }
}

export default StatsTab;

