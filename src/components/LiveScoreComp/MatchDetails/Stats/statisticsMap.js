const statisticsMap = [
    {
        label: 'Mbajtja e topit',
        home: 'homeBallPossession',
        away: 'awayBallPossession',
        formated: true,
    },
    {
        label: 'Rastet totale',
        home: 'homeTotalShotsOnGoal',
        away: 'awayTotalShotsOnGoal',
    },
    {
        label: 'Goditje ne porte',
        home: 'homeShotsOnGoal',
        away: 'awayShotsOnGoal',
    },
    {
        label: 'Goditje jashte porte',
        home: 'homeShotsOffGoal',
        away: 'awayShotsOffGoal',
    },
    {
        label: 'Goditjet e bllokuara',
        home: 'homeBlockedScoringAttempt',
        away: 'awayBlockedScoringAttempt',
    },
    {
        label: 'Korner',
        home: 'homeCornerKicks',
        away: 'awayCornerKicks',
    },
    {
        label: 'Pozicion jashteloje',
        home: 'homeOffsides',
        away: 'awayOffsides',
    },
    {
        label: 'Goditje denimi',
        home: 'homeFouls',
        away: 'awayFouls',
    },
    {   
        label: 'Kartona te verdhe',
        home: 'homeYellowCards',
        away: 'awayYellowCards',
    },
    {
        label: 'Raste per gol',
        home: 'homeBigChanceScored',
        away: 'awayBigChanceScored',
    },
    {
        label: 'Pritje te portierit',
        home: 'homeGoalkeeperSaves',
        away: 'awayGoalkeeperSaves',
    },
    {
        label: 'Pasime',
        home: 'homePasses',
        away: 'awayPasses',
    },
    {
        label: 'Pasime te sakta',
        home: 'homeAccuratePasses',
        away: 'awayAccuratePasses',
    },
    {
        label: 'Krosime',
        home: 'homeTotalCross',
        away: 'awayTotalCross',
    },
];

export default statisticsMap;
