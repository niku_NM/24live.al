// @flow
import React from 'react';
import { View } from 'react-native';
import LottieView from 'lottie-react-native';
import moment from 'moment';

import Text from '../../Global/Text';
import Theme from '../../Global/Theme';
import { withTheme } from '../../Global/Providers/ThemeProvider';
import CompetitionListWraper from '../../Competition/CompetitionListWraper';
import Venue from './HeaderComp/Venue';
import SingleTeam from './HeaderComp/SingleTeam';
import MidView from './HeaderComp/MidView';
import services from '../../../services';


class Header extends React.Component {
    renderStatusInfo() {
        const color = Theme.getTextColor(this.props.theme, 2);
        const white = Theme.getTextColor(this.props.theme);
        const green = Theme.getPrimaryColor(this.props.theme);
        const styles = {
            titleStyle: {
                color,
                fontSize: 12
            },
            container: {
                justifyContent: 'center',
                alignItems: 'center',
            }
        };

        switch (this.props.status) {
            case 'notstarted':
                return (
                    <View>
                        <Text numberOfLines={1} style={styles.titleStyle}>
                            Fillon ne
                        </Text>
                        <Text style={{ color: green, textAlign: 'center' }}>
                            {moment.unix(this.props.events.startTimestamp).local().format('HH:mm')}
                        </Text>
                    </View>
                );
            case 'inprogress':
                return (
                    <View style={[styles.container, { padding: 2 }]}>
                        <Text numberOfLines={1} style={styles.titleStyle}>
                            Duke luajtur
                        </Text>

                        <View style={[styles.container, { flexDirection: 'row' }]}>
                            <View
                                style={{
                                    position: 'absolute',
                                    top: 0,
                                    bottom: 0,
                                    left: -16,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <LottieView
                                    source={require('../../../assets/pulse_effect.json')}
                                    autoPlay
                                    loop
                                    style={{ height: 16, width: 16, }}
                                />
                            </View>    
                            
                            <Text style={{ color: white, }}>
                                {this.props.events.statusDescription}
                            </Text>
                        </View>

                    </View>
                );
            case 'finished':
                return (
                    <View >
                        <Text numberOfLines={1} style={styles.titleStyle}>
                            Perfunduar
                        </Text>
                        <Text style={{ color: green, textAlign: 'center' }}>
                            {this.props.events.statusDescription}
                        </Text>
                    </View>
                );
            default:
                return null;
        }
    }

    render() {
        const backgroundColor = Theme.getBgColor(this.props.theme, 2);
        const { 
            hasVenue, 
            venue, 
            formatedStartDate, 
            homeTeam, 
            awayTeam, 
            status, 
            homeScore, 
            awayScore 
        } = this.props.events;
        
        return (
            <View style={{ backgroundColor }}>
            <CompetitionListWraper style={{ paddingVertical: 10, marginHorizontal: 16, marginVertical: 4 }} >
                <Venue 
                    stadium={hasVenue ? venue.stadium.name : 'S\'ka Stadium'} 
                    date={formatedStartDate} 
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <SingleTeam
                        flag={services.teamLogoUrl(homeTeam.id)}
                        team={homeTeam.name}
                        fadeAnimation={this.props.fadeAnimation}
                    />
                    <View>
                        <MidView
                            status={status.type}
                            homeScoreCurrent={homeScore.current}
                            awayScoreCurrent={awayScore.current}
                        />
                        {this.renderStatusInfo()}
                    </View>
                    <SingleTeam
                        flag={services.teamLogoUrl(awayTeam.id)}
                        team={awayTeam.name}
                        fadeAnimation={this.props.fadeAnimation}
                    />
                </View>
            </CompetitionListWraper>
            </View>
        );
    }
}

export default withTheme(Header);
