import React from 'react';
import { View, FlatList } from 'react-native';
import { withTheme } from '../../../Global/Providers/ThemeProvider';

import CompetitionListWraper from '../../../Competition/CompetitionListWraper';
import Title from '../../../Global/Title';
import Formation from './Formation';
import Text from '../../../Global/Text';
import Icon from '../../../Global/Icon';
import Theme from '../../../Global/Theme';

class LineUp extends React.Component {
    static getDerivedStateFromProps(nextProps) {
        const { data } = nextProps;
        if (data && data.lineupsSorted) {
            const lineupsSorted = nextProps.data.lineupsSorted || [];
            return {
                lineup: lineupsSorted.filter(item => item.substitute === false),
                subs: lineupsSorted.filter(item => item.substitute === true),
                allData: lineupsSorted
            };
        }
        return null;
    }

    state = { lineup: [], subs: [], allData: [] };

    renderItem = ({ item }) => (
        <Formation {...item} />
    );

    renderEmpty() {
        return (
            <View style={styles.containerStyle}>
                <Icon
                    name='block'
                    size={40}
                    color={Theme.getPrimaryColor(this.props.theme)}
                />
                <Text
                    style={{
                        fontSize: 18,
                        color: Theme.getTextColor(this.props.theme, 2)
                    }}
                >S'ka te dhena</Text>
            </View>
        );
    }

    render() {
        return (
            <React.Fragment>
                {(this.state.allData.length === 0) ?
                    <View style={styles.containerStyle}>
                        <Icon
                            name='block'
                            size={60}
                            color={Theme.getPrimaryColor(this.props.theme)}
                        />
                        <Text
                            style={{
                                fontSize: 22,
                                color: Theme.getTextColor(this.props.theme, 2)
                            }}
                        >S'ka te dhena</Text>
                    </View> 
                    :
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ width: '50%' }}>
                            <CompetitionListWraper>
                                <Title title="Titullaret" />
                                <FlatList
                                    data={this.state.lineup}
                                    renderItem={this.renderItem}
                                    keyExtractor={(item, index) => `${item.shirtNumber || index}`}
                                    ListEmptyComponent={() => this.renderEmpty()}
                                />
                            </CompetitionListWraper>
                        </View>
                        <View style={{ width: '50%' }}>
                            <CompetitionListWraper>
                                <Title title="Stoli" />
                                <FlatList
                                    data={this.state.subs}
                                    renderItem={this.renderItem}
                                    keyExtractor={(item, index) => `${item.shirtNumber || index}`}
                                    ListEmptyComponent={() => this.renderEmpty()}
                                />
                            </CompetitionListWraper>
                        </View>
                    </View>
                }
            </React.Fragment>
        );
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        height: 200,
        justifyContent: 'center',
        alignItems: 'center'
    }
};

export default withTheme(LineUp);
