import React from 'react';
import { TabView, TabBar, PagerPan } from 'react-native-tab-view';

import Theme from '../../../Global/Theme';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import services from '../../../../services';
import LineUp from './LineUp';


class LineUpTabView extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: '1', title: 'Vendasit' },
            { key: '2', title: 'Miqte' },
        ],
        lineUps: {},
    };

    componentDidMount() {
        services.getLineUps(this.props.data)
            .then(lineUps => {
                this.setState({ lineUps });
            })
            .catch(error => console.log(error));
    }


    renderScene = ({ route }) => {
        switch (route.key) {
            case '1':
                return <LineUp data={this.state.lineUps.homeTeam} />;
            case '2':
                return <LineUp data={this.state.lineUps.awayTeam} />;
            default:
                return null;
        }
    };

    renderHeader = props =>
        <TabBar
            {...props}
            tabStyle={[
                styles.tabBar, 
                { backgroundColor: Theme.getBgColor(this.props.theme, 3),
                 borderBottomColor: Theme.getBgColor(this.props.theme, 3), 
                }
            ]}
            indicatorStyle={{ backgroundColor: 'transparent' }}
            labelStyle={{ color: Theme.getTextColor(this.props.theme) }}
            style={{ 
                backgroundColor: Theme.getBgColor(this.props.theme, 2),
                elevation: 0, 
                shadowOpacity: 0, 
            }}
        />;

    renderPagerPan = props =>
        <PagerPan
            {...props}
            swipeEnabled={false}
        />;

    render() {
        return (
            <TabView
                navigationState={this.state}
                renderScene={this.renderScene}
                onIndexChange={index => this.setState({ index })}
                renderTabBar={this.renderHeader}
                renderPager={this.renderPagerPan}
                style={{ backgroundColor: Theme.getBgColor(this.props.theme) }}
            />
        );
    }

}

const styles = {
    tabBar: {
        height: 44,
        borderBottomWidth: 2,
    }
};

export default withTheme(LineUpTabView);
