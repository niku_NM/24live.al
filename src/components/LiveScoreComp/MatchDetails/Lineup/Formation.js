// @flow
import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

import Theme from '../../../Global/Theme';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import Text from '../../../Global/Text';
import services from '../../../../services';

type Player = {
    name: string;
    slug: string;
    shortName: string;
    id: number;
    hasImage: boolean;
}

type Props = {
    position: number;
    shirtNumber: number;
    player: Player;
    substitute: boolean;
    positionName: string;
    positionNameshort: string;
    captain: boolean;
    icon: string;
    title: string;
    theme: 'dark' | 'light';
}


class Formation extends React.Component<Props, {}> {
    render() {
        const { player, shirtNumber, theme } = this.props;
        const color = Theme.getTextColor(theme);
        const grey = Theme.getTextColor(theme, 1);

        return (
            <View style={styles.containerStyle}>
                <Image
                    style={styles.imgStyle}
                    source={{
                        uri: services.playerLogoUrl(player.id) || services.noPlayerLogoUrl
                    }}
                />
                {shirtNumber && 
                <View>
                    <Text style={[styles.playerNumber, { color: grey }]}>
                        {shirtNumber}
                    </Text>
                </View>
                }
                {player &&
                    <Text
                        numberOfLines={1}
                        style={{ flex: 1, color }}
                    >
                        {player.shortName || player.name}
                    </Text>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        flexDirection: 'row',
        padding: 2,
        alignItems: 'center',
    },
    imgStyle: {
        width: 26,
        height: 26,
        borderRadius: 13,
        resizeMode: 'cover',
        marginLeft: 2
    },
    playerNumber: {
        width: 30,
        textAlign: 'center',
    }
});


export default withTheme(Formation);
