// @ flow
import React from 'react';
import { View, StyleSheet } from 'react-native';

import Theme from '../../../Global/Theme';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import Text from '../../../Global/Text';
import Icon from '../../../Global/Icon';
import CompetitionListWraper from '../../../Competition/CompetitionListWraper';
import Divider from '../../../Global/Divider';


type Props = {
    title: string;
    minute: string | number;
    playerName: string;
    playerInNumber: number | string;
    type: 'in' | 'out';
    playerOut: string;
    playerOutNumber: number | string;
    theme: 'dark' | 'light';
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 8,
        flex: 1
    },
});

const PlayerSub = withTheme(({ playerNumber, playerName, type, theme }) => (
    <View
        style={[
            styles.container,
            {
                flexDirection: type === 'in' ? 'row' : 'row-reverse',
                alignItems: type === 'in' ? 'flex-start' : 'flex-end'
            }
        ]}
    >
        <Icon name='swap-vert' size={22} color={type === 'in' ? '#09BA85' : '#BD0034'} />
        <Text
            style={{ color: Theme.getTextColor(theme, 1), paddingHorizontal: 6 }}
        >
            {playerNumber}
        </Text>
        <Text
            numberOfLines={1}
            style={{ color: Theme.getTextColor(theme) }}
        >
            {playerName}
        </Text>
    </View>
));

const SingleItem = withTheme((props: Props) => {
    const { minute, playerIn, playerInNumber, playerOut, playerOutNumber, } = props;
    return (
        <View style={{ flexDirection: 'row', paddingVertical: 8 }}>
            <PlayerSub
                playerNumber={playerInNumber}
                playerName={playerIn}
                type='in'
            />
            <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                <Icon name='keyboard-arrow-left' size={24} color="#09BA85" />
                <Text
                    style={{
                        paddingHorizontal: 10,
                        textAlign: 'center',
                        color: Theme.getTextColor(props.theme, 1)
                    }}
                >{minute}'
                </Text>
                <Icon name='keyboard-arrow-right' size={24} color="#BD0034" />
            </View>
            <PlayerSub
                playerNumber={playerOutNumber}
                playerName={playerOut}
                type='out'
            />
        </View>
    );
});


const Title = (props: Props) => (
    <View style={{ marginBottom: 6, justifyContent: 'center', alignItems: 'center', flex: 1 }}>
        <Text
            numberOfLines={1}
            style={{ fontSize: 16, color: Theme.getTextColor(props.theme, 2) }}
        >
            {props.title}
        </Text>
    </View>
);

class Substitutions extends React.Component {
    render() {
        return (
            <CompetitionListWraper style={{ padding: 6 }}>
                <Title title='Substitutions' />
                <SingleItem
                    minute={73}
                    playerIn='C.Ronaldo'
                    playerOut='Messi'
                    playerInNumber={7}
                    playerOutNumber={10}
                />
                <Divider />
                <SingleItem
                    minute={73}
                    playerIn='C.Ronaldo'
                    playerOut='Messi'
                    playerInNumber={7}
                    playerOutNumber={10}
                />
                <Divider />
                <SingleItem
                    minute={73}
                    playerIn='C.Ronaldo'
                    playerOut='Messi'
                    playerInNumber={7}
                    playerOutNumber={10}
                />
            </CompetitionListWraper>
        );
    }
}

export default withTheme(Substitutions);

