// @flow

import React from 'react';
import { View, StyleSheet } from 'react-native';

import Text from '../../../Global/Text';
import Theme from '../../../Global/Theme';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import CompetitionListWraper from '../../../Competition/CompetitionListWraper';
import Title from '../../../Global/Title';
import Divider from '../../../Global/Divider';

type Props = {
  label: string;
  percentage: number;
  value: string | number;
  theme: 'dark' | 'light'
}

const SingleBar = ({ label, percentage, barColor, value, theme }: Props) => (
  <View style={[styles.container, { backgroundColor: Theme.getBgColor(theme, 1) }]}>
    <View style={{ backgroundColor: Theme.getBgColor(theme), padding: 12, width: 48 }}>
      <Text
        style={{ fontWeight: '600', color: Theme.getTextColor(theme), textAlign: 'center' }}
      >{label}</Text>
    </View>
    <View style={{ flex: 1, marginHorizontal: 8 }}>
      <View
        style={{
          backgroundColor: barColor,
          width: `${percentage}%`,
          height: 24,
          borderRadius: 24,
        }}
      />
    </View>
    <View style={{ padding: 12, width: 74 }}>
      <Text
        style={{ color: Theme.getTextColor(theme), fontWeight: '600', textAlign: 'center', }}
      >
        {Math.round(value)} %
      </Text>
    </View>
  </View>
);

class PredictionBar extends React.Component<{}, {}> {
  render() {
    const {
      vote1ScaledPercentage,
      voteXScaledPercentage,
      vote2ScaledPercentage,
      vote1Percentage,
      voteXPercentage,
      vote2Percentage,
    } = this.props.votes;
    return (
      <View>
        <Title title="Parashikimi i ndeshjes" />
        <CompetitionListWraper>
          <SingleBar
            theme={this.props.theme}
            label="1"
            percentage={vote1ScaledPercentage}
            value={vote1Percentage}
            barColor='#09BA85'
          />
          <Divider />
          <SingleBar
            theme={this.props.theme}
            label="X"
            percentage={voteXScaledPercentage}
            value={voteXPercentage}
            barColor='#097EBA'
          />
          <Divider />
          <SingleBar
            theme={this.props.theme}
            label="2"
            percentage={vote2ScaledPercentage}
            value={vote2Percentage}
            barColor='#9809BA'
          />
        </CompetitionListWraper>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden'
  }
});

export default withTheme(PredictionBar);
