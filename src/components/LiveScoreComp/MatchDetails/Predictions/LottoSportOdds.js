// @flow

import React from 'react';
import { View, StyleSheet } from 'react-native';

import Text from '../../../Global/Text';
import Theme from '../../../Global/Theme';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import CompetitionListWraper from '../../../Competition/CompetitionListWraper';
import Title from '../../../Global/Title';

type Props = {
    label: string;
    odd: number;
    theme: 'dark' | 'light'
};

const SingleOdd = ({ label, odd, theme }: Props) => (
    <View style={styles.container}>
        <Text style={[styles.text, { color: Theme.getTextColor(theme) }]}>
            {label}
        </Text>
        <Text style={[styles.odds, { color: Theme.getTextColor(theme) }]}>
            {odd}
        </Text>
    </View>
);

class LottoSportOdds extends React.Component<{}, {}> {
    render() {
        return (
            <View>
                <Title title='LottoSport Odds' />
                <CompetitionListWraper>
                    <View style={{ justifyContent: 'space-around', flexDirection: 'row' }}>
                        <SingleOdd label='1' theme={this.props.theme} odd={2.42} />
                        <SingleOdd label='X' theme={this.props.theme} odd={14.1} />
                        <SingleOdd label='2' theme={this.props.theme} odd={9.2} />
                    </View>
                </CompetitionListWraper>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 22,
        width: 60
    },
    text: {
        paddingBottom: 14,
        fontWeight: '600',
        fontSize: 20,
        width: '100%',
        textAlign: 'center',
    },
    odds: {
        borderColor: '#09BA85',
        borderRadius: 8,
        borderWidth: 1.5,
        paddingHorizontal: 12,
        paddingVertical: 4,
        fontWeight: '600',
        fontSize: 16,
        width: '100%',
        textAlign: 'center',
    }
});

export default withTheme(LottoSportOdds);

