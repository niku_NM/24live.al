import React from 'react';
import { Svg, Path } from 'react-native-svg';

const YellowRedCard = ({ size= 24, ...rest }) => (
    <Svg width={size} height={size} {...rest} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <Path fill="#ffc10e" d="M17.3 1H2.7A1.57 1.57 0 0 0 1 2.39v19.22A1.57 1.57 0 0 0 2.7 23h14.6a1.57 1.57 0 0 0 1.7-1.39V2.39A1.57 1.57 0 0 0 17.3 1z" />
      <Path fill="#ec1c24" d="M21.31 1H6.7A1.57 1.57 0 0 0 5 2.39v19.22A1.57 1.57 0 0 0 6.7 23h14.6a1.57 1.57 0 0 0 1.7-1.39V2.39A1.57 1.57 0 0 0 21.31 1z" />
    </Svg>
  );
  

export default YellowRedCard;
