import React from 'react';
import { Svg, Path } from 'react-native-svg';

const Substitution = ({ size = 24, ...rest }) => (
    <Svg width={size} height={size} {...rest} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <Path fill="#179721" d="M19 14V1L5 7.51z" />
      <Path fill="#ec1c24" d="M5 10v13l14-6.51z" />
    </Svg>
);

export default Substitution;
