import React from 'react';
import { Svg, Path } from 'react-native-svg';

const Injury = ({ size = 24, ...rest }) => (
<Svg width={size} height={size} {...rest} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
    <Path fill="#ec1c24" d="M2.71,2.84V2.12H2v.71Zm18.57,0H22V2.12h-.71Zm0,18.33v.71H22v-.71Zm-18.57,0H2v.71h.71Zm0-17.61H21.29V2.12H2.71Zm17.86-.71V21.16H22V2.84Zm.71,17.61H2.71v1.43H21.29Zm-17.86.71V2.84H2V21.16Z" />
    <Path fill="#ec1c24" d="M10.29,14H6.09V10.81h4.2V6.65h3.3v4.15h4.2V14h-4.2v4.18h-3.3Z" />
</Svg>
);

export default Injury;
