import React from 'react';
import { Svg, Path } from 'react-native-svg';

const KickOff = ({ size = 24, theme, ...rest }) => (
    <Svg width={size} height={size} {...rest} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <Path fill="#fff" d="M18.57 12.52A3.44 3.44 0 1 1 22 9.08a3.44 3.44 0 0 1-3.43 3.44z" />
        <Path fill="#3a3a3a" d="M17.8 10l-.3-1a.31.31 0 0 1 .11-.35l.77-.57a.31.31 0 0 1 .37 0l.77.57a.31.31 0 0 1 .11.35l-.3.93a.31.31 0 0 1-.3.22h-1a.31.31 0 0 1-.23-.15zM18.39 6.77a.31.31 0 0 0 .37 0l1-.71V6a3.15 3.15 0 0 0-2.26 0zM16.34 8.54a.31.31 0 0 0 .11-.35L16.09 7a3.24 3.24 0 0 0-.71 2v.13zM17.38 11.05a.31.31 0 0 0-.3-.22H15.9a3.26 3.26 0 0 0 1.83 1.33zM21.26 10.89h-1.19a.32.32 0 0 0-.3.22l-.36 1.14a3.24 3.24 0 0 0 1.85-1.36zM20.69 8.19a.31.31 0 0 0 .11.35l1 .7v-.13a3.26 3.26 0 0 0-.71-2z" />
        <Path fill="#3a3a3a" d="M18.57 12.52A3.44 3.44 0 1 1 22 9.08a3.44 3.44 0 0 1-3.43 3.44zm0-6.51a3.07 3.07 0 1 0 3.05 3.07A3.07 3.07 0 0 0 18.57 6z" />
        <Path fill={theme === 'dark' ? '#fff' : '#000'} d="M12.13 16.8l-1.28-.8-.3.94a.09.09 0 0 0 0 .11l.82.46a.09.09 0 0 0 .12 0zM5.9 13.07l-1.29-.78-.33 1a.09.09 0 0 0 0 .11l.82.46a.09.09 0 0 0 .12 0zm8.25 4.55a5.74 5.74 0 0 1-1.45-.49l-.29.91a.09.09 0 0 0 0 .11l.82.46a.09.09 0 0 0 .12 0l.75-.9a.1.1 0 0 0 .06-.09zM2.2 9.28c-.44.81-.13 1.1.57 1.5l.67.37.55.33c.9.57 7.39 4.5 8.71 5.24s3.37 1 4-.06-1.31-2.07-2.16-3.41a16.79 16.79 0 0 0-2.69-2.92c.1-.48-.37-.8-.55-.9h-.07c.13-.57-.7-1-.7-1l-1.31-2.9a.25.25 0 0 0-.1-.12.25.25 0 0 0-.23 0l-1.14.5C7 6.24 8.6 8 8.26 8.12a1.82 1.82 0 0 1-1.16-.29 3.1 3.1 0 0 1-.82-.68c-.53-.62 0-1.56-.28-1.74-.11-.06-.32 0-.71.12-1.55.65-2.44 2.56-3.09 3.75zm5.5 3l4-.9a.1.1 0 0 1 .08 0l.7.69a.09.09 0 0 1 0 .15l-3.58.83h-.12l-1.09-.61a.09.09 0 0 1 .02-.17zM5.51 11l4.9-1a.1.1 0 0 1 .08 0l.7.68a.09.09 0 0 1 0 .16l-4.43 1h-.11l-1.16-.64a.09.09 0 0 1 .02-.2zm-2.91.06L2.46 11l-.33 1a.09.09 0 0 0 0 .11l.82.46a.09.09 0 0 0 .12 0l.69-.83-.52-.31-.37-.2z" />
    </Svg>
);

export default KickOff;
