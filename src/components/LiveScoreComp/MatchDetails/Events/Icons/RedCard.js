import React from 'react';
import { Svg, Path } from 'react-native-svg';

const RedCard = ({ size = 24, ...rest }) => (
    <Svg width={size} height={size} {...rest} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <Path fill="#ec1c24" d="M19.31 1H4.7A1.57 1.57 0 0 0 3 2.39v19.22A1.57 1.57 0 0 0 4.7 23h14.6a1.57 1.57 0 0 0 1.7-1.39V2.39A1.57 1.57 0 0 0 19.31 1z" />
    </Svg>
);

export default RedCard;
