// @flow
import React from 'react';

import Events from './Events';
import { type ThemeProps } from '../../../Global/Providers/ThemeProvider';


type Props = ThemeProps & {
    eventType: number;
    size?: number|string;
}

const NoIcon = () => null;

const EventIcon = (props: Props) => {
    const { eventType, ...rest } = props;
    const Icon = Events.icons[eventType] || NoIcon;

    return <Icon {...rest} />;
};

export default EventIcon;
