import { 
    KickOff,
    Goal,
    AutoGoal,
    EndExtraTime,
    EndFirstHalf,
    EndMatch,
    FailPenallty,
    InteruptMatch,
    RedCard,
    RestartMatch,
    StartExtraTime,
    StartExtraTime2,
    StartSecondHalf,
    Substitution,
    SuccesPenallty,
    SuccesPenalltyExtra,
    YellowCard,  
    FailPenalltyExtra,
    YellowRedCard,
} from './Icons';

const EVENTS = {
    1: 'Fillon ndeshja',
    2: 'Gol',
    3: 'Autogol',
    5: 'Karton i kuq',
    6: 'Karton i verdhe',
    7: 'Zevendesim',
    8: 'Penallti e shenuar',
    9: 'Penallti e humbur',
    11: 'Mbaron pjesa e pare ( HT )',
    12: 'Fillon pjesa e dyte',
    13: 'Mbaron ndeshja ( FT )',
    14: 'Nderpritet ndeshja ( SUSP )',
    15: 'Fillon extra time ( EXTRA )',
    17: 'Fillon P2 extra time ( EXTRA )',
    18: 'Mbaron EXTRA TIME',
    21: 'Penallti e shenuar ( PEN EXTRA )',
    22: 'Penallti e humbur ( PEN EXTRA )',
    23: 'Rifillon ndeshja'
};

const EVENTS_COMPONENTS = {
    1: KickOff,
    2: Goal,
    3: AutoGoal,
    5: RedCard,
    6: YellowCard,
    7: Substitution,
    8: SuccesPenallty,
    9: FailPenallty,
    11: EndFirstHalf,
    12: StartSecondHalf, //'Fillon pjesa e dyte',
    13: EndMatch, //'Mbaron ndeshja ( FT )',
    14: InteruptMatch, //'Nderpritet ndeshja ( SUSP )',
    15: StartExtraTime, //'Fillon extra time ( EXTRA )',
    17: StartExtraTime2, //'Fillon P2 extra time ( EXTRA )',
    18: EndExtraTime, //'Mbaron EXTRA TIME',
    21: SuccesPenalltyExtra, //'Penallti e shenuar ( PEN EXTRA )',
    22: FailPenalltyExtra, //'Penallti e humbur ( PEN EXTRA )',
    23: RestartMatch, ///'Rifillon ndeshja'
    24: YellowRedCard
};

export default {
    labels: EVENTS,
    icons: EVENTS_COMPONENTS,
};
