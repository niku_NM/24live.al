// @flow
import React from 'react';
import { View } from 'react-native';

import { withTheme } from '../../../Global/Providers/ThemeProvider';
import Theme from '../../../Global/Theme';
import Text from '../../../Global/Text';

type Props = {
    stadium: string;
    date: string;
    theme: 'dark' | 'light';
}

const Venue = (props:Props) => {
    const { stadium, date, theme } = props;
    const color = Theme.getTextColor(theme);
    const dateColor = Theme.getTextColor(theme, 2);

    return (
        <View style={styles.containter}>
        <Text
            numberOfLines={1} 
            style={{ color, }}
        >
            {stadium}
        </Text>
        <Text
            numberOfLines={1}  
            style={{ color: dateColor, fontSize: 12 }}
        >
            {date}
        </Text>
    </View>
    );
};

const styles = {
    containter: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
};

export default withTheme(Venue);
