// @flow
import React from 'react';
import { View } from 'react-native';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import Theme from '../../../Global/Theme';
import Text from '../../../Global/Text';

type Props = {
    status: 'notstarted' | 'inprogress' | 'finished';
    homeScoreCurrent: number;
    awayScoreCurrent: number;
    theme: 'dark' | 'light';
}

const MidView = (props: Props) => {
    const { status, theme, homeScoreCurrent, awayScoreCurrent } = props;
    const color = Theme.getPrimaryColor(theme);
    const grey = Theme.getTextColor(theme, 1);
    const styles = {
        containter: {
            height: 58,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 10
        },
        scoreStyle: {
            fontSize: 22, 
            letterSpacing: 4, 
            color, 
            textAlign: 'center'
        }
    };

    return (
        <View style={styles.containter}>
            {status === 'inprogress' | status === 'finished' ?
                <Text style={styles.scoreStyle}>{homeScoreCurrent}-{awayScoreCurrent}</Text>
                :
                <Text style={{ textAlign: 'center', color: grey, fontSize: 12 }}>VS</Text>
            }
        </View>
    );
};

export default withTheme(MidView);
