// @flow
import React from 'react';
import { View } from 'react-native';
import FastImage from 'react-native-fast-image';

import { withTheme } from '../../../Global/Providers/ThemeProvider';
import Theme from '../../../Global/Theme';
import Text from '../../../Global/Text';

type Props = {
    flag: string;
    team: string;
    theme: 'dark' | 'light';
}

const SingleTeam = (props: Props) => {
    const { flag, team, theme } = props;
    const color = Theme.getTextColor(theme);
    const styles = {
        imgStyle: {
            width: 58,
            height: 58,
            marginBottom: 10,
            overflow: 'hidden'
        },
        textStyle: {
            textAlign: 'center',
            fontSize: 14,
            fontWeight: '400',
            color,
            marginHorizontal: 6
        }
    };

    return (
        <View style={{ alignItems: 'center', flex: 1 }} >
            <FastImage
                style={styles.imgStyle}
                source={{ uri: flag }}
            />
            <Text numberOfLines={1} style={styles.textStyle}>
                {team}
            </Text>
        </View>
    );
};

export default withTheme(SingleTeam);
