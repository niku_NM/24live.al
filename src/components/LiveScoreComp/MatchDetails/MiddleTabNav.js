import * as React from 'react';
import {
  StyleSheet,
  Dimensions,
  Animated,
  View
} from 'react-native';
import { TabBar, TabView } from 'react-native-tab-view';

import Theme from '../../Global/Theme';
import { withTheme } from '../../Global/Providers/ThemeProvider';
import PredictionBar from './Predictions/PredictionBar';
import H2HTabView from './HeadToHead/H2HTabView';
import LineUpTabView from './Lineup/LineUpTabView';
import MatchEvents from './MatchInfo/MatchEvents';
import StatsTab from './Stats/StatsTab';
import Header from './Header';
import TeamStandings from './Standings/TeamStandings';


const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

const HEADER_HEIGHT = 224;
const COLLAPSED_HEIGHT = 54;
const SCROLLABLE_HEIGHT = HEADER_HEIGHT - COLLAPSED_HEIGHT;


class MiddleTabNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: '1', title: 'Kronologjia' },
        { key: '2', title: 'Statistikat' },
        { key: '3', title: 'Formacioni' },
        { key: '4', title: 'Perballjet' },
        { key: '5', title: 'Parashikimet' },
        { key: '6', title: 'Klasifikimi' },
      ],
      scroll: new Animated.Value(0),
    };
  }

  onIndexChange = index => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo({ x: 0, y: 0, animated: true });
    }
    Animated.timing(
      // Animate value over time
      this.state.scroll, // The value to drive
      {
        duration: 100,
        toValue: 0,
        useNativeDriver: true,
      }
    ).start();
    this.setState({ index });
  }

  $renderScene = ({ route }) => {
    switch (route.key) {
      case '1':
        return (
            <MatchEvents events={this.props.incidentsData} />
        );
      case '2':
        return (
            <StatsTab stats={this.props.statisticsData} />
        );
      case '3':
        return (
            <LineUpTabView data={this.props.matchId} />
        );
      case '4':
        return (
            <H2HTabView data={this.props.matchId} />
        );
      case '5':
        return (
            <PredictionBar votes={this.props.predictionData} />
        );
      case '6':
        return (
            <TeamStandings data={this.props.headerData} />
        );
      default:
        return null;
    }
  }

  renderScene = (...params) => (
    <Animated.ScrollView
      ref={ref => { this.scrollViewRef = ref ? ref._component : null; }} // eslint-disable-line
      scrollEventThrottle={1}
      onScroll={Animated.event(
        [{ nativeEvent: { contentOffset: { y: this.state.scroll } } }],
        { useNativeDriver: true }
      )}
      contentContainerStyle={{ paddingTop: HEADER_HEIGHT }}
    >
      <View style={{ marginBottom: 6, }}>
        {this.$renderScene(...params)}
      </View>
    </Animated.ScrollView>
  )

  renderHeader = props => {
    const translateY = this.state.scroll.interpolate({
      inputRange: [0, SCROLLABLE_HEIGHT],
      outputRange: [0, -SCROLLABLE_HEIGHT],
      extrapolate: 'clamp',
    });

    return (
      <Animated.View style={[styles.header, { transform: [{ translateY }] }]}>
        <Header events={this.props.headerData} status={this.props.headerData.status.type} />
        <TabBar
          scrollEnabled
          {...props}
          indicatorStyle={{ backgroundColor: '#09BA85', height: 1 }}
          labelStyle={{ color: Theme.getTextColor(this.props.theme), fontSize: 13 }}
          style={{ backgroundColor: Theme.getBgColor(this.props.theme, 2), elevation: 0, }}
        />
      </Animated.View>
    );
  };

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this.renderScene}
        onIndexChange={this.onIndexChange}
        renderTabBar={this.renderHeader}
        style={{ backgroundColor: Theme.getBgColor(this.props.theme) }}
        initialLayout={initialLayout}
      />
    );
  }
}

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1,
  },
});

export default withTheme(MiddleTabNav);

