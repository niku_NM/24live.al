// @flow

import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';

import { withTheme } from '../../../Global/Providers/ThemeProvider';
import CompetitionListWraper from '../../../Competition/CompetitionListWraper';
import Period from './Incidents/Period';
import SubstitutionIncident from './Incidents/SubstitutionIncident';
import PenaltyIncident from './Incidents/PenaltyIncident';
import InjuryTime from './Incidents/InjuryTime';
import GoalIncident from './Incidents/GoalIncident';
import CardIncident from './Incidents/CardIncident';
import Icon from '../../../Global/Icon';
import Text from '../../../Global/Text';
import Theme from '../../../Global/Theme';


class MatchEvents extends React.Component {
    renderItem = ({ item }) => {
        switch (item.incidentType) {
            case 'period':
                return <Period {...item} />;
            case 'substitution':
                return <SubstitutionIncident {...item} />;
            case 'penalty':
                return <PenaltyIncident {...item} />;
            case 'injuryTime':
                return <InjuryTime {...item} />;
            case 'goal':
                return <GoalIncident {...item} />;
            case 'card':
                return <CardIncident {...item} />;
            default:
                return null;
        }
    }

    renderEmpty() {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, padding: 10 }}>
                <Icon 
                    name='sentiment-dissatisfied' 
                    size={40} 
                    color={Theme.getPrimaryColor(this.props.theme)} 
                />
                <Text 
                    style={[styles.textStyle, { color: Theme.getTextColor(this.props.theme) }]}
                >S'ka te dhena per ndeshjen</Text>
            </View>
        );
    }

    render() {
        return (
            <CompetitionListWraper>
            <FlatList 
                data={this.props.events}
                renderItem={this.renderItem}
                ListHeaderComponent={() =>
                    (this.props.events.length ? <Period text='FILLON NDESHJA' /> : null)
                }
                keyExtractor={(item, index) => `${item.id || index}`}
                ListEmptyComponent={() => this.renderEmpty()}                
            />
            </CompetitionListWraper>
        );
    }
}

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 20, 
        textAlign: 'center',
    }
});

export default withTheme(MatchEvents);
