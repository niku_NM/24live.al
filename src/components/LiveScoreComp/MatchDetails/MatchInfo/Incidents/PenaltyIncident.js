// @flow
import React from 'react';
import { View } from 'react-native';

import { FailPenallty } from '../../Events/Icons';
import { withTheme } from '../../../../Global/Providers/ThemeProvider';
import Theme from '../../../../Global/Theme';
import Text from '../../../../Global/Text';
import VerticalDivider from './VerticalDivider';

type IPlayer = {
    name: string;
    slug: string;
    shortName: string;
    id: number;
}
type IPenaltyIncident = {
    scoringTeam: number;
    description: string;
    missed: number;
    id: string;
    time: number;
    addedTime: number;
    player: IPlayer;
    incidentType: "penalty";
    isHome: boolean;
    incidentDescription: string;
    incidentClass: "missedpenalty";
    timeSpecial: number;
}

type Props = IPenaltyIncident;

class PenaltyIncident extends React.Component<Props, {}> {

    render() {
        const { theme, isHome, time, player } = this.props;
        const color = Theme.getTextColor(theme);

        return (
            <View
                style={{
                    flexDirection: isHome ? 'row' : 'row-reverse',
                    alignItems: 'center',
                    paddingVertical: 6,
                    marginHorizontal: 2
                }}
            >
                <FailPenallty theme={theme} size={18} />
                <Text
                    numberOfLines={1}
                    style={{
                        paddingHorizontal: 6,
                        color: Theme.getTextColor(theme, 1)
                    }}
                >
                    {time}'
                </Text>
                {player && 
                <Text numberOfLines={1} style={{ color, maxWidth: '28%', }}>
                    {player.shortName || player.name}
                </Text>
                }
                <VerticalDivider />
            </View>
        );
    }
}

export default withTheme(PenaltyIncident);
