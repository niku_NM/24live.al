// @flow
import React from 'react';
import { View } from 'react-native';

import { Substitution } from '../../Events/Icons';
import { withTheme } from '../../../../Global/Providers/ThemeProvider';
import Theme from '../../../../Global/Theme';
import Text from '../../../../Global/Text';
import VerticalDivider from './VerticalDivider';


type IPlayer = {
    name: string;
    slug: string;
    shortName: string;
    id: number;
}
type ISubIncident = {
    playerIn: IPlayer;
    playerOut: IPlayer;
    playerTeam: 1 | 2; // 
    id: string;
    time: number;
    incidentType: "substitution";
    isHome: boolean;
    incidentDescription: string;
    incidentClass: "substitutionin";
    timeSpecial: number;
}

type Props = ISubIncident;

class SubstitutionIncident extends React.Component<Props, {}> {
    render() {
        const { theme, playerTeam, time, playerIn, playerOut } = this.props;
        const color = Theme.getTextColor(theme);
        const grey = Theme.getTextColor(theme, 1);

        return (
            <View
                style={{
                    flexDirection: playerTeam === 1 ? 'row' : 'row-reverse',
                    alignItems: 'center',
                    paddingVertical: 6,
                    marginHorizontal: 2,
                }}
            >
                <Substitution theme={theme} size={18} />
                <Text
                    numberOfLines={1}
                    style={{
                        paddingHorizontal: 6,
                        color: Theme.getTextColor(theme, 1)
                    }}
                >
                    {time}'
                </Text>

                <View style={{ maxWidth: '28%' }} >
                    {playerIn &&
                        <Text numberOfLines={1} style={{ color, }}>
                            {playerIn.shortName || playerIn.name}
                        </Text>
                    }
                    {playerOut &&
                        <Text numberOfLines={1} style={{ color: grey, }}>
                            {playerOut.shortName || playerOut.name}
                        </Text>
                    }
                </View>
                <VerticalDivider />
            </View>
        );
    }
}

export default withTheme(SubstitutionIncident);
