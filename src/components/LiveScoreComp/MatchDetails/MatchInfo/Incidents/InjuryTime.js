// @flow
import React from 'react';
import { View } from 'react-native';

import { withTheme } from '../../../../Global/Providers/ThemeProvider';
import Theme from '../../../../Global/Theme';
import Text from '../../../../Global/Text';
import ExtraTime from '../../Events/Icons/ExtraTime';


type IInjuryTime = {
    length: number;
    time: number;
    text: string;
    incidentType: "injuryTime";
    isHome: boolean;
    incidentDescription: string;
    incidentClass: "injuryTime";
    timeSpecial: number;
}

type Props = IInjuryTime;

class InjuryTime extends React.Component<Props, {}> {

    render() {
        const { theme, length } = this.props;
        const color = Theme.getTextColor(theme, 1);
        const backgroundColor = Theme.getBgColor(theme, 1);

        return (
            <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingVertical: 6,
                }}
            >
              <View
                    style={[
                        styles.lineStyle,
                        { backgroundColor: theme === 'dark' ? '#454E5D' : '#E6DDDD', }]}
              />
              <View style={{ flexDirection: 'row', backgroundColor }}>
                <ExtraTime theme={theme} />
                <Text
                    numberOfLines={1}
                    style={{
                        paddingHorizontal: 6,
                        color
                    }}
                >
                    Kohe shtese + {length}'
                </Text>
                </View>
            </View>
        );
    }
}

const styles = {
    lineStyle: {
        position: 'absolute',
        alignSelf: 'center',
        width: 4,
        height: 50,
        backgroundColor: 'red',
        overflow: 'visible'
    }
};

export default withTheme(InjuryTime);

