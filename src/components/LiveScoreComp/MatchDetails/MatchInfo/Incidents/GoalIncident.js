// @flow
import React from 'react';
import { View } from 'react-native';

import { Goal, SuccesPenallty, AutoGoal } from '../../Events/Icons';
import { withTheme } from '../../../../Global/Providers/ThemeProvider';
import Theme from '../../../../Global/Theme';
import Text from '../../../../Global/Text';
import VerticalDivider from './VerticalDivider';


type IPlayer = {
    name: string;
    slug: string;
    shortName: string;
    id: number;
}
type IGoalIncident = {
    homeScore: number;
    awayScore: number;
    assist1: IPlayer;
    scoringTeam: 1 | 2;
    playerTeam: 1 | 2;
    id: string;
    time: number;
    player: IPlayer;
    incidentType: "goal";
    isHome: boolean;
    incidentDescription: string;
    incidentClass: "penalty" | "regulargoal" | "owngoal";
    timeSpecial: number;
}

type Props = IGoalIncident;

class GoalIncident extends React.Component<Props, {}> {
    renderIcon() {
        switch (this.props.incidentClass) {
            case 'penalty':
                return <SuccesPenallty theme={this.props.theme} size={18} />;
            case 'regulargoal':
                return <Goal theme={this.props.theme} size={18} />;
            case 'owngoal':
                return <AutoGoal theme={this.props.theme} size={18} />;
            default:
                break;
        }
    }

    render() {
        const { theme, playerTeam, time, player } = this.props;
        const color = Theme.getTextColor(theme);

        return (
            <View
                style={{
                    flexDirection: playerTeam === 1 ? 'row' : 'row-reverse',
                    alignItems: 'center',
                    paddingVertical: 6,
                    marginHorizontal: 2
                }}
            >
                {this.renderIcon()}
                <Text
                    numberOfLines={1}
                    style={{
                        paddingHorizontal: 6,
                        color: Theme.getTextColor(theme, 1)
                    }}
                >
                    {time}'
                </Text>

                {player &&
                    <Text numberOfLines={1} style={{ color, maxWidth: '28%', }}>
                        {player.shortName || player.name}
                    </Text>
                }
                <VerticalDivider />
            </View>
        );
    }
}

export default withTheme(GoalIncident);

