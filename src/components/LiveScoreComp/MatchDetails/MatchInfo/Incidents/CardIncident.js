// @flow
import React from 'react';
import { View } from 'react-native';

import { YellowCard, RedCard, YellowRedCard } from '../../Events/Icons';
import { withTheme } from '../../../../Global/Providers/ThemeProvider';
import Theme from '../../../../Global/Theme';
import Text from '../../../../Global/Text';
import VerticalDivider from './VerticalDivider';

type IPlayer = {
    name: string;
    slug: string;
    shortName: string;
    id: number;
}
type ICardIncident = {
    type: string;
    playerTeam: 1 | 2; // 
    reason: string;
    id: string;
    time: number;
    addedTime: number;
    player: IPlayer;
    incidentType: "card";
    isHome: boolean;
    incidentDescription: string;
    incidentClass: "yellowcard" | "redcard" | "yellowcard2";
    timeSpecial: number;
}

type Props = ICardIncident;

class CardIncident extends React.Component<Props, {}> {
    renderIcon() {
        switch (this.props.incidentClass) {
            case 'yellowcard':
                return <YellowCard theme={this.props.theme} size={18} />;
            case 'redcard':
                return <RedCard theme={this.props.theme} size={18} />;
            case 'yellowcard2':
                return <YellowRedCard theme={this.props.theme} size={18} />;
            default:
                break;
        }
    }

    render() {
        const { theme, playerTeam, time, player } = this.props;
        const color = Theme.getTextColor(theme);

        return (
            <View
                style={{
                    flexDirection: playerTeam === 1 ? 'row' : 'row-reverse',
                    alignItems: 'center',
                    paddingVertical: 6,
                    marginHorizontal: 2,
                }}
            >
                {this.renderIcon()}
                <Text
                    numberOfLines={1}
                    style={{
                        paddingHorizontal: 6,
                        color: Theme.getTextColor(theme, 1)
                    }}
                >
                    {time}'
                </Text>

                {player &&
                <Text numberOfLines={1} style={{ color, maxWidth: '28%', }}>
                    {player.shortName || player.name}
                </Text>
                }
                <VerticalDivider />
            </View>
        );
    }
}

export default withTheme(CardIncident);

