import React from 'react';
import { View, StyleSheet } from 'react-native';
import { withTheme } from '../../../../Global/Providers/ThemeProvider';
import Theme from '../../../../Global/Theme';

const styles = StyleSheet.create({
    deviderStyle: {
        position: 'absolute',
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center'
    },
    lineStyle: {
        width: 4,
        flex: 1
    },
    circleStyle: {
        width: 18,
        height: 18,
        borderRadius: 9,
        borderWidth: 2,
        elevation: 8
    }
});

const VerticalDivider = (props) => {
    const { theme } = props;
    const borderColor = Theme.getBgColor(theme, 1);

    return (
        <React.Fragment>
        <View style={styles.deviderStyle}>
            <View
                style={[
                    styles.lineStyle,
                    { backgroundColor: theme === 'dark' ? '#454E5D' : '#E6DDDD', }]}
            />
        </View>

        <View style={styles.deviderStyle}>
            <View
                style={[
                    styles.circleStyle,
                    { backgroundColor: theme === 'dark' ? '#6B7B8F' : '#F9F2F2', borderColor }]}
            />
        </View>
        </React.Fragment>
    );
};

export default withTheme(VerticalDivider);
