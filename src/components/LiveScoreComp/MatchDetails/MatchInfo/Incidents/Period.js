// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';

import { withTheme } from '../../../../Global/Providers/ThemeProvider';
import Theme from '../../../../Global/Theme';
import Text from '../../../../Global/Text';


type IPeriodIncident = {
    text: string;
    incidentType: "period";
    incidentDescription: string;
    incidentClass: "period";
    timeSpecial: string | number;
}

type Props = IPeriodIncident;

class Period extends React.Component<Props, {}> {

    render() {
        const { theme, text } = this.props;
        const backgroundColor = Theme.getBgColor(theme, 2);
        const color = Theme.getTextColor(theme, 1);
        // const green = Theme.getPrimaryColor(theme);

        return (
            <View style={{ height: 42, backgroundColor, justifyContent: 'center' }}>
                <View
                    style={[
                        styles.lineStyle,
                        { backgroundColor: theme === 'dark' ? '#454E5D' : '#E6DDDD', }]}
                />
                <View style={[styles.labelContainer, { backgroundColor, }]}>
                    <Text style={{ color, fontWeight: '500' }} numberOfLines={1}>
                        {text}
                </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    labelContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 8,
        overflow: 'hidden',
        paddingHorizontal: 6,
        paddingVertical: 6,
        // borderWidth: 1,
    },
    lineStyle: {
        position: 'absolute',
        alignSelf: 'center',
        width: 4,
        height: 42,
        backgroundColor: 'red',
        overflow: 'visible'
    },
});

export default withTheme(Period);
