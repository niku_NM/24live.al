import React from 'react';
import { TabView, TabBar, PagerPan } from 'react-native-tab-view';

import Theme from '../../../Global/Theme';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import LastGames from './LastGames';
import services from '../../../../services';
import H2HLastGames from './H2HLastGames';

class H2HTabView extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: '1', title: 'Vendasit' },
            { key: '2', title: 'KMK' },
            { key: '3', title: 'Miqte' },
        ],
        lastGames: {}
    };

    componentDidMount() {
        services.getLastGames(this.props.data)
            .then(lastGames => {
                this.setState({ lastGames });
            })
            .catch(error => console.log(error));
    }

    renderScene = ({ route }) => {
        switch (route.key) {
            case '1':
                return <LastGames data={this.state.lastGames.home} />;
            case '2':
                return <H2HLastGames data={this.state.lastGames.h2h} />;
            case '3':
                return <LastGames data={this.state.lastGames.away} />;
            default:
                return null;
        }
    };

    renderHeader = props =>
        <TabBar
            {...props}
            tabStyle={[
                styles.tabBar,
                { backgroundColor: Theme.getBgColor(this.props.theme, 3),
                borderBottomColor: Theme.getBgColor(this.props.theme, 3), 
            }]}
            indicatorStyle={{ backgroundColor: 'transparent' }}
            labelStyle={{ color: Theme.getTextColor(this.props.theme) }}
            style={{ 
                backgroundColor: Theme.getBgColor(this.props.theme, 2),
                elevation: 0,
                shadowOpacity: 0, 
            }}
        />;

    renderPagerPan = props =>
        <PagerPan
            {...props}
            swipeEnabled={false}
        />;

    render() {
        return (
            <TabView
                navigationState={this.state}
                renderScene={this.renderScene}
                onIndexChange={index => this.setState({ index })}
                renderTabBar={this.renderHeader}
                renderPager={this.renderPagerPan}
                style={{ backgroundColor: Theme.getBgColor(this.props.theme) }}
            />
        );
    }
}

const styles = {
    tabBar: {
        height: 44,
        borderBottomWidth: 2,
    }
};

export default withTheme(H2HTabView);
