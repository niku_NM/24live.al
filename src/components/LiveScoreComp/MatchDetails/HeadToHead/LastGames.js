import _ from 'lodash';
import React from 'react';
import { FlatList } from 'react-native';
import { withNavigation } from 'react-navigation';

import { withTheme } from '../../../Global/Providers/ThemeProvider';
import Encounters from '../../../Competition/Encounters/EncountersItem';
import services from '../../../../services';
import CompetitionListWraper from '../../../Competition/CompetitionListWraper';
import Divider from '../../../Global/Divider';


function flatEvents(events = []) {
    if (!Array.isArray(events)) {
        return [];
    }
    const newEvents = events.map(e => e.events);
    return _.flattenDeep(newEvents);
}

class LastGames extends React.Component {
    static getDerivedStateFromProps(nextProps) {
        const { data } = nextProps;
        if (data && data.recent) {
            return { events: flatEvents(nextProps.data.recent.tournaments || []) };
        }
        return null;
    }
    state = { events: [] }

    onItemPress = (item) => {
        this.props.navigation.push('MatchDetails', { id: item.id });
    }

    renderItem = ({ item }) => {
        const event = item;

        return (
            <Encounters
                homeTeam={event.homeTeam.name}
                homeTeamFlag={services.teamLogoUrl(event.homeTeam.id)}
                awayTeam={event.awayTeam.name}
                awayTeamFlag={services.teamLogoUrl(event.awayTeam.id)}
                homeScore={event.homeScore.normaltime}
                awayScore={event.awayScore.normaltime}
                homeScoreCurrent={event.homeScore.current}
                awayScoreCurrent={event.awayScore.current}
                homeScoreP1={event.homeScore.period1}
                awayScoreP1={event.awayScore.period1}
                status={event.status.type}
                statusDescription={event.statusDescription}
                showDate={event.formatedStartDate}
                matchId={event.id}
                hideStar
                onPress={() => this.onItemPress(item)}
            />
        );
    }

    render() {
        return (
            <CompetitionListWraper>
            <FlatList
                data={this.state.events}
                renderItem={this.renderItem}
                ItemSeparatorComponent={() => <Divider />}
                keyExtractor={(item, index) => `${item.id || index}`}
            />
            </CompetitionListWraper>
        );
    }
}

export default withTheme(withNavigation(LastGames));
