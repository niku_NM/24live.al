// @flow
import React from 'react';
import { View } from 'react-native';

import { withTheme } from '../../../Global/Providers/ThemeProvider';
import Theme from '../../../Global/Theme';
import Text from '../../../Global/Text';

type Props = {
    number: number | string;
    theme: 'dark' | 'light'
}

const NumberCell = (props:Props) => (
    <View style={{ width: 24, marginHorizontal: 6 }}>
    <Text 
        style={{ textAlign: 'center', color: Theme.getTextColor(props.theme) }}
    >
    {props.number}
    </Text>
    </View>
);

export default withTheme(NumberCell);
