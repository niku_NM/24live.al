// @flow
import React from 'react';
import { View, Image } from 'react-native';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import Theme from '../../../Global/Theme';
import Text from '../../../Global/Text';
import NumberCell from './NumberCell';
import services from '../../../../services';
import Promotion from './Promotion';

type Team = {
    name: string;
    slug: string;
    national: boolean,
    id: number;
    shortName: string;
}

type Fields = {
    matchesTotal: string;
    winTotal: string;
    drawTotal: string;
    lossTotal: string;
    goalsTotal: string;
    goalDiffTotal: string;
    pointsTotal: string;
}

type Props = {
    id: number;
    team: Team;
    position: string;
    homePosition: string;
    awayPosition: string;
    points: string;
    homePoints: string;
    awayPoints: string;
    totalFields: Fields;
}

const styles = {
    img: {
        width: 20,
        height: 20,
        marginRight: 12,
        marginLeft: 6
    },
    container: {
        flexDirection: 'row',
        paddingVertical: 6,
        height: 30
    },
    container1: {
        width: '64%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    container2: {
        width: '36%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    }
};

const SingleRow = (props: Props) => {
    const { team, position, points, totalFields, theme, homeTeamId, awayTeamId, promotion } = props;
    const color = Theme.getTextColor(theme);
    const green = Theme.getPrimaryColor(theme);
    return (
        <View style={styles.container}>
            <View style={styles.container1}>
                    {team.id === homeTeamId || team.id === awayTeamId ? 
                       <View style={{ width: 5, backgroundColor: green, height: 30, }} />
                        : 
                       <View style={{ width: 5 }} /> 
                    }

                    <View style={{ width: 25 }}>
                        { promotion && props.showPromotion && <Promotion id={promotion.id} /> }

                        <Text style={{ color, textAlign: 'center', }}>
                            {position}
                        </Text>
                    </View>
                <Image
                    source={{ uri: services.teamLogoUrl(team.id) }}
                    style={styles.img}
                />
                <Text
                    numberOfLines={1}
                    style={{ color, fontSize: 16, fontWeight: '400', width: '64%' }}
                >
                    {team.name}
                </Text>
            </View>
            <View style={styles.container2}>
                <NumberCell number={totalFields.winTotal} />
                <NumberCell number={totalFields.drawTotal} />
                <NumberCell number={totalFields.lossTotal} />
                <View style={{ width: 30, borderLeftColor: 'black', borderLeftWidth: 1, }}>
                    <Text style={{ textAlign: 'center', color }}>{points}</Text>
                </View>
            </View>
        </View>
    );
};

export default withTheme(SingleRow);
