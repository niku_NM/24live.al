import React from 'react';
import { FlatList, ScrollView, View, ActivityIndicator } from 'react-native';

import CompetitionListWraper from '../../../Competition/CompetitionListWraper';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import Divider from '../../../Global/Divider';
import TableHeader from './TableHeader';
import SingleRow from './SingleRow';
import services from '../../../../services';
// import Theme from '../../../Global/Theme';
import Text from '../../../Global/Text';
import Header from '../../../Global/Header/Header';

class Standings extends React.Component {
    state = {
        standingsData: {},
        table: [],
        loading: false,
    };

    componentDidMount() {
        this.setState({ loading: true, });
        if (!this.props.data.season) return this.setState({ loading: false });
        const tournamentId = this.props.data.tournament.id;
        const seasonId = this.props.data.season.id;
        return (
        services.getStandings(tournamentId, seasonId)
            .then(standingsData => {
                this.setState({
                    loading: false,
                    standingsData: standingsData || {},
                    table: standingsData.standingsTables[0] ? 
                    standingsData.standingsTables[0].tableRows : []
                });
            })
            .catch(error => {
                console.log(error);
                this.setState({ loading: false, table: [] });
            })
        );
    }

    renderItem = (data) => {
        const { item } = data;
        const { homeTeam, awayTeam } = this.props.data;
        const homeTeamId = homeTeam.id;
        const awayTeamId = awayTeam.id;
        return (
            <SingleRow {...item} homeTeamId={homeTeamId} awayTeamId={awayTeamId} />
        );
    }

    renderEmpty() {
		if (this.props.loading) {
			return (
				<View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
					<ActivityIndicator size={40} color="red" />
					<Text style={{ color: 'white', fontWeight: 'bold' }} > loading </Text>
				</View>
			);
		}

		return (
			<View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
				<Text style={{ fontWeight: 'bold', color: 'red' }} >S'ka te dhena</Text>
			</View>
		);
	}


    render() {
        return (
            <ScrollView style={{ flex: 1, }}>
                <CompetitionListWraper>
                    <FlatList
                        data={this.state.table || []}
                        renderItem={this.renderItem}
                        ListHeaderComponent={() => <TableHeader />}
                        ItemSeparatorComponent={() => <Divider />}
                        keyExtractor={(item, index) => `${item.id || index}`}
                        ListEmptyComponent={() => this.renderEmpty()}
                    />
                </CompetitionListWraper>
            </ScrollView>
        );
    }
}

const StandingsWithTheme = withTheme(Standings);

StandingsWithTheme.navigationOptions = () => {
    return {
      title: 'Klasifikimi',
      header: Header,
      headerRight: (
          <View style={{ width: 30 }} />
      )
    };
  };

  export default StandingsWithTheme;

