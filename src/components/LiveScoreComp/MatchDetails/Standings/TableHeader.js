import React from 'react';
import { View } from 'react-native';

import Text from '../../../Global/Text';
import { withTheme } from '../../../Global/Providers/ThemeProvider';
import Theme from '../../../Global/Theme';
import NumberCell from './NumberCell';

const TableHeader = (props) => {
    const color = Theme.getTextColor(props.theme);
    const backgroundColor = Theme.getBgColor(props.theme, 2);
    const styles = {
        headerStyle: {
            width: '36%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end'
        }
    };

    return (
        <React.Fragment>
            <View style={{ flexDirection: 'row', backgroundColor, }}>
                <View style={{ width: '64%', justifyContent: 'center' }}>
                    <View style={{ width: 30 }}>
                        <Text style={{ textAlign: 'center', color }}>#</Text>
                    </View>
                </View>
                <View style={styles.headerStyle}>
                    <NumberCell number='F' />
                    <NumberCell number='B' />
                    <NumberCell number='H' />
                    <View style={{ width: 30 }}>
                        <Text style={{ textAlign: 'center', color }}>P</Text>
                    </View>
                </View>
            </View>
        </React.Fragment>
    );
};

export default withTheme(TableHeader);
