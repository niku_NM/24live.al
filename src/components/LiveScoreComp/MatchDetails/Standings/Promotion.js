// @flow
import React from 'react';
import { View } from 'react-native';

type Props = {
    id: number;
    name: string;
}

const colors = {
    green: '#19AA7E',
    lightgreen: '#39DB44', 
    darkblue: '#2144CF',
    blue: '#47ABF6',
    red: '#D41A40', 
    orange: '#F59D39', 
    grey: '#8296AF',
};

const Circle = ({ backgroundColor }) => (
    <View style={{ width: 20, height: 20, borderRadius: 10, backgroundColor }} />
);


const colorMap = {
    1: colors.green, //Champions League
    2: colors.lightgreen, //Champions League Qualification
    4: colors.blue, //UEFA Cup Qualification
    5: colors.green, //Promotion
    6: colors.lightgreen, //Promotion Playoffs
    7: colors.red, //Relegation
    8: colors.orange, //Relegation Playoffs
    9: colors.green, //Playoffs
    16: colors.green, //Copa Libertadores
    17: colors.lightgreen, //Copa Sudamericana
    18: colors.grey, //CAF Confederation Cup
    23: colors.green, //Relegation Round
    25: colors.grey, //AFC Champions League
    26: colors.darkblue, //europaLegaue
    27: colors.blue, //europa legaue qualifications
    28: colors.orange, //Relegation Playoffs
    29: colors.lightgreen, //Promotion Playoff
    30: colors.green, //Semifinal
    33: colors.lightgreen, //Next group phase
    36: colors.green, //AFC Cup
    40: colors.green, //Finals
    44: colors.lightgreen, //Championship round
    45: colors.darkblue, //Copa Libertadores Qualification
};

function getColor(id) {
    return colorMap[id];
}

const Promotion = (props:Props) => (
    <View style={{ position: 'absolute', alignSelf: 'center' }}>
    <Circle backgroundColor={getColor(props.id)} />
    </View>
);


export default Promotion;
