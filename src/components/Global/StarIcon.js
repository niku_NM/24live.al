import React from 'react';
import { ButtonIcon } from './Button';

class StarIcon extends React.Component {
    state = { active: false };

    setIconActive(active) {
        this.setState({ active });
    }

    toggleIcon = () => {
        this.setIconActive(!this.state.active);
    }

    get icon() {
        if (this.state.active) {
            return 'star';
        }

        return 'star-border';
    }

    render() {
        return (
            <ButtonIcon icon={this.icon} flat onPress={this.toggleIcon} />
        );
    }
}

export default StarIcon;
