// @flow
import React from 'react';
import { View } from 'react-native';
import Text from '../Global/Text';
import Theme from './Theme';
import { withTheme } from '../Global/Providers/ThemeProvider';

type Props = {
    title: string | React.Component<any> | (props: any) => React.Component<any>;
    titleProps: any;
    theme: 'light' | 'dark';
}

class Title extends React.Component<Props, {}> {
    static defaultProps = {
        titleProps: {},
        theme: 'dark'
    }

    render() {
        const { title, theme } = this.props;
        const borderBottomColor = Theme.getTextColor(theme, 2);
        const color = Theme.getTextColor(theme);
        return (
            <View style={{ marginHorizontal: 10, marginBottom: 6 }}>
                <Text
                    {...this.props}
                    style={[{
                        color,
                        fontWeight: 'bold',
                        borderBottomWidth: 1,
                        borderBottomColor,
                        padding: 8,
                        fontSize: 16,
                    }, this.props.style]}
                >
                    {title}
                </Text>
            </View>  
        );
    }
}

export default withTheme(Title);
