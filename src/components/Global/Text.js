import * as React from 'react';
import { Text } from 'react-native';

const GlobalText = (props) => <Text {...props} />;

export default GlobalText;

