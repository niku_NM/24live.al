// @flow
import * as React from 'react';
import firebase from 'react-native-firebase';
// import update from 'react-addons-update';
import Favorite from '../../utils/FavoriteFB';
import services from '../../services';


type Action = 'add' | 'remove';

const defaultProps = {
    favorites: {},
    matches: [],
    loading: false,
    refreshing: false,
    modifyFavorite: () => {},
    refreshMatches: async () => {},
};

const FavoriteContext = React.createContext(defaultProps);

class FavoriteProviderNew extends React.Component<any, any> {
    constructor(props) {
      super(props);
      this.state = {
        favorites: {},
        matches: [],
        loading: false,
        refreshing: false,
        modifyFavorite: this.modifyFavorite,
        refreshMatches: this.refreshMatches,
      };
    }

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            if (!user) {
                return;
            }
            this.listenForMatches();
        });
    }

    listenForMatches() {
        const uid = firebase.auth().currentUser.uid;
        firebase.firestore().collection(`users/${uid}/matchFavorites`)
            .onSnapshot(async querySnapshot => {
                const favorites = {};
                querySnapshot.forEach(snap => {
                    favorites[snap.id] = snap.data();
                });

                const matchIds = [];
                const removeMatchIds = [];
                querySnapshot.docChanges.forEach(snap => {
                    if (snap.type === 'added') {
                        matchIds.push(snap.doc.id);
                    }

                    if (snap.type === 'removed') {
                        removeMatchIds.push(snap.doc.id);
                    }
                });
                const matches = await this.loadMatchByIds(matchIds);
                console.log({ removeMatchIds, favorites }, querySnapshot, 'querySnapshot');
                console.log(this.removeMatchByIds(this.state.matches, removeMatchIds));
                this.setState(prevState => ({
                    matches: [
                        ...this.removeMatchByIds(prevState.matches, removeMatchIds),
                        ...matches,
                    ],
                    favorites,
                }));
            });
    }

    async loadMatchByIds(matchIds = []) {
        const promises = matchIds
        .map(id => services.getMatchData(id).catch(() => null));

        const matches = await Promise.all(promises);
        return matches.filter(m => !!m).map(m => m.event).filter(m => !!m);
    }

    refreshMatches = async () => {
        this.setState({
            refreshing: true,
        });
        try {
            const matchIds = Object.keys(this.state.favorites);
            const matches = await this.loadMatchByIds(matchIds);
            this.setState({
                refreshing: false,
                matches,
            });
        } catch (error) {
            console.log(error);
            this.setState({
                refreshing: false,
            });
        }
    }

    removeMatchByIds(matches = [], removeMatchIds = []) {
        return matches.filter(match => !removeMatchIds.includes(String(match.id)));
    }

    modifyFavorite = async (id, action: Action) => {
        this.setState({
            loading: id
        });
        try {
            if (action === 'add') {
                await Favorite.saveMatch(id);
            }
            if (action === 'remove') {
                await Favorite.removeMatch(id);
            }
            this.setState({
                loading: false,
            });
        } catch (error) {
            console.log(error);
            this.setState({
                loading: false,
            });
        }
    }
  
    render() {
        return (
            <FavoriteContext.Provider value={this.state}>
                {this.props.children}
            </FavoriteContext.Provider>
        );
    }
}

export function withFavorites(Comp: React.ComponentType<any>) {
    return (props: any) => (
      <FavoriteContext.Consumer>
        {({ modifyFavorite, matches, favorites, loading, refreshing, refreshMatches }) => (
          <Comp 
            {...props}
            loading={loading}
            refreshing={refreshing}
            matches={matches}
            favorites={favorites}
            modifyFavorite={modifyFavorite}
            refreshMatches={refreshMatches}
          />
        )}
      </FavoriteContext.Consumer>
    );
  }
  

export default FavoriteProviderNew;
  
