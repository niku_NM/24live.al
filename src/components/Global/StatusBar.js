import * as React from 'react';
import { StatusBar } from 'react-native';

import Theme from './Theme';
import { withTheme } from './Providers/ThemeProvider';


const ThemedStatusBar = (props) => (
  <StatusBar
    backgroundColor={Theme.getBgColor(props.theme, 2)}
    barStyle={props.theme === 'light' ? 'dark-content' : 'light-content'}
  />
);

export default withTheme(ThemedStatusBar);

