// @flow
import * as React from 'react';
import {
  View,
  ActivityIndicator,
  StyleSheet,
  type TextStyle,
  type ViewStyle,
} from 'react-native';
import Text from '../Text';
import Theme from '../Theme';
import Icon from '../Icon';

import TouchableItem from './TouchableItem';

import { ButtonIcon } from './ButtonIcon';

type Props = {
  title: ?string,
  icon: ?string,
  iconProps?: { color: string, size: number },
  onPress?: () => void,
  delayPressIn?: number,
  borderless?: boolean,
  pressColor?: string,
  activeOpacity?: number,
  style?: ViewStyle,
  titleStyle?: TextStyle,
  size?: 'default' | 'small' | 'large',
  type?: 'default' | 'primary' | 'accent',
  flat: ?boolean,
  loading?: boolean,
  disabled: boolean,
}

function getSize(size: string): number {
  if (size === 'small') {
    return 32;
  }

  if (size === 'large') {
    return 48;
  }

  return 36;
}

const shadow = {
  shadowColor: 'rgba(0,0,0,1)',
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.2,
};

function getColor(type: string, flat: boolean = false, disabled: boolean = false, theme): string {
  const th = Theme.getTheme(theme);
  let color = th.primary;
  let flatColor = th.primary;
  if (type === 'primary') {
    color = 'white';
    flatColor = th.primary;
  }

  if (type === 'accent') {
    color = 'white';
    flatColor = th.accent;
  }

  if (disabled) {
    return th.gray;
  }

  return flat ? flatColor : color;
}


function getBgColor(type: string, flat: boolean = false, disabled: boolean = false, theme): string {
  const th = Theme.getTheme(theme);
  let color = 'rgba(255,255,255,1)';
  const flatColor = 'rgba(0,0,0,0)';
  if (type === 'primary') {
    color = th.primary;
  }

  if (type === 'accent') {
    color = th.accent;
  }

  if (disabled) {
    return flat ? flatColor : th.lightenGray;
  }

  return flat ? flatColor : color;
}

const Button = ({
  onPress,
  title,
  icon,
  style,
  titleStyle,
  size,
  type,
  iconProps,
  flat,
  loading,
  disabled,
  ...rest
}: Props) => (
    // $FlowFixMe
    <TouchableItem
      onPress={onPress}
      disabled={disabled || loading}
      style={[{
        backgroundColor: getBgColor(type, flat, disabled),
        height: getSize(size),
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16,
        borderRadius: 4,
        elevation: flat ? 0 : 3,
        ...(flat ? {} : shadow),
      }, style]}
      {...rest}
    >
      {icon &&
        <Icon
          name={icon}
          provider={iconProps.provider}
          size={iconProps.size || 24}
          color={iconProps.color || getColor(type, flat, disabled)}
          style={{
            opacity: loading ? 0 : 1,
          }}
        />
      }
      {!!title &&
        <Text
          style={[{ color: getColor(type, flat, disabled), opacity: loading ? 0 : 1 }, titleStyle]}
          numberOfLines={1}
        >
          {title}
        </Text>
      }
      {loading &&
        <View style={[StyleSheet.absoluteFill, { justifyContent: 'center', alignItems: 'center' }]} >
          <ActivityIndicator
            // size={iconProps.size || 24}
            color={iconProps.color || getColor(type, flat, disabled)}
          />
        </View>
      }
    </TouchableItem>
  );

Button.defaultProps = {
  onPress: () => { },
  borderless: false,
  delayPressIn: 0,
  pressColor: '',
  activeOpacity: 0.5,
  style: {},
  titleStyle: {},
  size: 'default',
  type: 'default',
  iconProps: {},
  loading: false,
};

Button.Icon = ButtonIcon;
Button.TouchableItem = TouchableItem;

export default Button;
