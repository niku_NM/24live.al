// @flow
import * as React from 'react';
import {
  View,
  // $FlowFixMe
  type ViewStyle,
} from 'react-native';

import Theme from '../Theme';
import { withTheme } from '../Providers/ThemeProvider';
import Icon from '../Icon';
import TouchableItem from './TouchableItem';

type Props = {
  icon: ?string;
  iconProps: { color: string, size: number, style?: ViewStyle };
  onPress?: () => void;
  delayPressIn?: number;
  borderless?: boolean;
  pressColor?: string;
  activeOpacity?: number;
  style?: ViewStyle;
  size?: 'default' | 'small' | 'large';
  type?: 'default' | 'primary' | 'accent';
  theme: 'light' | 'dark';
  flat: boolean;
  round: ?boolean;
  disabled: boolean;
}

function getSize(size?: string): number {
  if (size === 'small') {
    return 32;
  }

  if (size === 'large') {
    return 48;
  }

  return 36;
}

const shadow = {
  shadowColor: 'rgba(0,0,0,1)',
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.2,
};

function getColor(
  type?: string,
  flat: boolean = false,
  disabled: boolean = false,
  theme: 'light' | 'dark'
): string {
  const th = Theme.getTheme(theme);
  let color = th.primary;
  let flatColor = th.primary;

  if (disabled) {
    return 'white';
  }

  if (type === 'primary') {
    color = 'white';
    flatColor = th.primary;
  }

  if (type === 'default') {
    color = th.background.primary;
    flatColor = th.text.primary;
  }

  if (type === 'accent') {
    color = 'white';
    flatColor = th.accent;
  }
  return flat ? flatColor : color;
}


function getBgColor(
  type?: string,
  flat?: boolean = false,
  disabled: boolean = false,
  theme: 'light' | 'dark'
): string {
  const th = Theme.getTheme(theme);
  let color = 'rgba(255,255,255,1)';
  const flatColor = 'rgba(0,0,0,0)';
  if (disabled) {
    return '#E4E4E4';
  }

  if (type === 'primary') {
    color = th.primary;
  }

  if (type === 'accent') {
    color = th.accent;
  }

  return flat ? flatColor : color;
}

const ButtonIcon = ({
  onPress,
  icon,
  style,
  size,
  type,
  iconProps,
  flat,
  round,
  disabled,
  theme = 'dark',
  ...rest
}: Props) => (
    // $FlowFixMe
    <TouchableItem
      onPress={onPress}
      style={[{
        backgroundColor: getBgColor(type, flat, disabled, theme),
        width: getSize(size),
        height: getSize(size),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 2,
        borderRadius: round ? getSize(size) / 2 : 4,
        elevation: (flat || disabled) ? 0 : 3,
        ...((flat || disabled) ? {} : shadow),
      }, style]}
      disabled={disabled}
      {...rest}
    >
      <View
        style={[
          {
            alignItems: 'center',
            justifyContent: 'center',
          },
          iconProps.style,
        ]}
      >
        <Icon
          name={icon}
          size={iconProps.size || 24}
          color={iconProps.color || getColor(type, flat, disabled, theme)}
        />
      </View>
    </TouchableItem>
  );

ButtonIcon.defaultProps = {
  onPress: () => { },
  borderless: undefined,
  delayPressIn: 0,
  pressColor: null,
  activeOpacity: 0.5,
  style: {},
  size: 'default',
  type: 'default',
  iconProps: {},
  flat: false,
  disabled: false,
};

export { ButtonIcon }; 
export default withTheme(ButtonIcon);
