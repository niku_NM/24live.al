import * as React from 'react';
import { Header, type HeaderProps } from 'react-navigation';

import Theme from '../Theme';
import { withTheme } from '../Providers/ThemeProvider';


const CustomHeader = (props: HeaderProps) => {
  const newProps: HeaderProps = { ...props };
  newProps.scene.descriptor.options.headerTintColor = Theme.getTextColor(props.theme);
  if (!newProps.scene.descriptor.options.headerStyle) {
    newProps.scene.descriptor.options.headerStyle = {};
  }
  newProps.scene.descriptor.options.headerStyle.backgroundColor = Theme.getBgColor(props.theme, 2);
  return (
    <Header
      {...props}
    />
  );
};

export default withTheme(CustomHeader);

