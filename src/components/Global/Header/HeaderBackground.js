import * as React from 'react';
import { View, StyleSheet } from 'react-native';

import Theme from '../Theme';
import { withTheme } from '../Providers/ThemeProvider';


const HeaderBackground = (props) => (
  <View
    style={[StyleSheet.absoluteFill, props.style]}
    backgroundColor={Theme.getBgColor(props.theme, 2)}
    barStyle={props.theme === 'light' ? 'dark-content' : 'light-content'}
  />
);

export default withTheme(HeaderBackground);

