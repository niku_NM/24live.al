// @flow
import * as React from 'react';
import { type IconProps } from 'react-native-vector-icons';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';


type Props = IconProps;
const Icon = (props: Props) => <MaterialIcon {...props} />;

export default Icon;
