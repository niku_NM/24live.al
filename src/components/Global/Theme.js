// @flow

export type ThemeTypes = 'light' | 'dark';

const dark = {
  primary: '#09BA85',
  accent: '#9809BA',
  text: {
    primary: '#FFFFFF',
    second: '#D7DDEB',
    third: '#6B7B8F',
  },
  background: {
    darkest: '#0F141E',
    darker: '#263146',
    dark: '#1F2A3F',
    primary: '#1A2537',
  },
  button: {
    pressColor: 'rgba(255,255,255,0.5)',
  },
};

const light = {
  primary: '#09BA85',
  accent: '#9809BA',
  text: {
    primary: '#1F2A3F',
    second: 'rgba(0,0,0,0.75)',
    third: 'rgba(0,0,0,0.5)',
  },
  background: {
    darkest: '#FFFFFF',
    darker: '#E3E4E4',
    dark: '#F9F9F9',
    primary: '#F4F6F8',
  },
  button: {
    pressColor: 'rgba(0,0,0,0.25)',
  },
};

const Theme = {
  light,
  dark,
};

function getTextColor(theme: ThemeTypes, darkness: 0 | 1 | 2 = 0) {
  const th = Theme[theme] || Theme.dark;
  if (darkness === 1) {
    return th.text.second;
  }

  if (darkness === 2) {
    return th.text.third;
  }

  return th.text.primary;
}

function getBgColor(theme: ThemeTypes, darkness: 0 | 1 | 2 | 3 = 0) {
  const th = Theme[theme] || Theme.dark;
  if (darkness === 1) {
    return th.background.dark;
  }

  if (darkness === 2) {
    return th.background.darkest;
  }

  if (darkness === 3) {
    return th.background.darker;
  }

  return th.background.primary;
}


function getPrimaryColor(theme: ThemeTypes) {
  return (Theme[theme] || Theme.dark).primary;
}

function getAccentColor(theme: ThemeTypes) {
  return (Theme[theme] || Theme.dark).accent;
}

function getButtonPressColor(theme: ThemeTypes) {
  return (Theme[theme] || Theme.dark).button.pressColor;
}

function getTheme(theme: ThemeTypes) {
  return (Theme[theme] || Theme.dark);
}

export default {
  light,
  dark,
  getTextColor,
  getPrimaryColor,
  getAccentColor,
  getBgColor,
  getButtonPressColor,
  getTheme,
};
