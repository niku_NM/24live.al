// @flow
import _ from 'lodash';
import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from './Icon';
import Text from './Text';
import Theme, { type ThemeTypes } from './Theme';


type IconProps = {
    size: number;
    style: Object;
    color: string;
} | {}

type Props = {
    icon: string | React.Component<any> | (props: any) => React.Component<any>;
    iconProps: IconProps;
    title: string | React.Component<any> | (props: any) => React.Component<any>;
    titleProps: any;
    theme: ThemeTypes;
    direction: 'ltr' | 'rtl';
    containerStyle: Object;
    style: Object;
}

const styles = StyleSheet.create({
    defaultTitleStyle: {
        fontWeight: 'bold',
        fontSize: 15,
    }
});

class IconInfo extends React.Component<Props, {}> {
    static defaultProps = {
        iconProps: {},
        titleProps: {},
        theme: 'dark',
        direction: 'ltr',
    }

    renderIcon() {
        const { icon, iconProps, theme } = this.props;
        if (typeof icon === 'string') {
            const color = Theme.getPrimaryColor(theme);
            return (
                <Icon name={icon} color={color} {...iconProps} />
            );
        }

        if (_.isFunction(icon)) {
            const IconComp = icon;
            // $FlowFixMe
            return <IconComp {...iconProps} theme={theme} />;
        }

        return icon;
    }

    renderTitle() {
        const { title, titleProps, theme } = this.props;
        const color = Theme.getTextColor(theme);
        if (typeof title === 'string') {
            return (
                <Text
                    {...titleProps}
                    numberOfLines={1}
                    style={[styles.defaultTitleStyle, { color }, titleProps.style]}
                >
                    {title}
                </Text>
            );
        }

        if (_.isFunction(title)) {
            const Title = title;
            return (
                // $FlowFixMe
                <Title
                    numberOfLines={1}
                    {...titleProps}
                    style={[styles.defaultTitleStyle, { color }, titleProps.style]}
                />
            );
        }

        return title;
    }
    
    render() {
        const flexDirection = this.props.direction === 'rtl' ? 'row-reverse' : 'row';
        const textAlign = this.props.direction === 'rtl' ? 'flex-end' : 'flex-start';
        const width = this.props.padding || 8; 
        return (
            <View style={this.props.containerStyle}>
                <View style={[{ flexDirection, alignItems: 'center' }, this.props.style]}>
                    <View>
                        {this.renderIcon()}
                    </View>
                    <View style={{ width }} />
                    <View style={{ flex: 1, alignItems: textAlign }}>
                        {this.renderTitle()}
                    </View>
                </View>
            </View> 
        );
    }
}

export default IconInfo;
