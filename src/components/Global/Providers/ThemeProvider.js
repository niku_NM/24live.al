// @flow
import * as React from 'react';


type ThemeTypes = 'light' | 'dark';

export type ThemeProps = {
  theme: ThemeTypes;
}

const themeDefaultProps: ThemeProps = {
  theme: 'dark',
  toggleTheme: () => {}
};

// $FlowFixMe
const ThemeContext = React.createContext(themeDefaultProps);

type Props = {
  children: React.Node;
  theme: ThemeTypes;
}

class ThemeProvider extends React.Component<Props, ThemeProps> {
  static getDerivedStateFromProps(nextProps: Props, prevState: ThemeProps) {
    if (prevState.theme !== nextProps.theme) {
      return {
        theme: nextProps.theme,
      };
    }

    return null;
  }

  constructor(props) {
    super(props);
    this.state = {
      theme: 'dark',
      toggleTheme: this.toggleTheme,
    };
  }


  toggleTheme = () => {
    this.setState((prevState) => ({
      theme: prevState.theme === 'dark' ? 'light' : 'dark'
    }));
  };

  render() {
    return (
      <ThemeContext.Provider value={this.state}>
        {this.props.children}
      </ThemeContext.Provider>
    );
  }
}

export function withTheme(Comp: React.ComponentType<any>) {
  return (props: any) => (
    <ThemeContext.Consumer>
      {({ theme, toggleTheme }) => (
        <Comp {...props} theme={theme} toggleTheme={toggleTheme} />
      )}
    </ThemeContext.Consumer>
  );
}


export default ThemeProvider;
