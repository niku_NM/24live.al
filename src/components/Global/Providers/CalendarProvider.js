import * as React from 'react';
import moment from 'moment';
import services from '../../../services';

const calendarDefaultProps = {
    changeDate: () => { },
    currentDate: {},
    tournaments: [],
};

const CalendarContext = React.createContext(calendarDefaultProps);

type Props = {
    children: React.Node;
}

class CalendarProvider extends React.Component<Props, {}> {
    constructor(props) {
        super(props);
        this.state = {
            tournaments: [],
            loading: false,
            refreshing: false,
            lastUpdate: null,
            refreshCompetitions: this.refreshCompetitions,
            changeDate: this.changeDate,
            currentDate: moment().format('YYYY-MM-DD'),
        };
    }

    componentDidMount() {
        this.setState({ loading: true });
        services.getCompetitions(this.state.currentDate)
            .then(categories => {
                this.setState({ 
                    tournaments: this.formatTournaments(categories.sportItem.tournaments),
                    lastUpdate: moment().unix(),
                    loading: false, 
                });
            })
            .catch(error => {
                console.log(error);
                this.setState({ loading: false });
            });
    }

    refreshCompetitions = () => {
        this.setState({ refreshing: true, });
        services.getCompetitions(this.state.currentDate)
        .then(categories => {
            this.setState({ 
                tournaments: this.formatTournaments(categories.sportItem.tournaments),
                lastUpdate: moment().unix(),
                refreshing: false
             });
        })
        .catch(error => {
            console.log(error);
        });
    }

    changeDate = (date) => {
        this.setState({
            currentDate: date,
            loading: true,
        });
        services.getCompetitions(date)
            .then(categories => {
                this.setState({
                    tournaments: this.formatTournaments(categories.sportItem.tournaments, date),
                    lastUpdate: moment().unix(),
                    loading: false
                });
            })
            .catch(error => {
                console.log(error);
                this.setState({ loading: false, tournaments: [] });
            });
    }

    formatTournaments(tournaments = [], date = null) {
        if (!Array.isArray(tournaments)) {
            return [];
        }

        if (date && moment(date).format('DD-MM-YYYY') !== moment().format('DD-MM-YYYY')) {
            return tournaments;
        }

        return tournaments.map(tournament => {
            const newTour = { ...tournament };
            newTour.events = tournament.events.filter(match => {
                const today = moment();
                const eventDate = moment.unix(match.startTimestamp);
                if (Number(eventDate.format('DD')) < Number(today.format('DD'))) {
                    return false;
                }

                return true;
            });
            return newTour;
        }).filter(tournament => !!tournament.events.length);
    }

    render() {
        return (
            <CalendarContext.Provider value={this.state}>
                {this.props.children}
            </CalendarContext.Provider>
        );
    }
}

export function withDateUpdated(Comp: React.ComponentType<any>) {
    return (props: any) => (
        <CalendarContext.Consumer>
            {({ 
                currentDate, 
                changeDate, 
                tournaments, 
                loading, 
                refreshing, 
                refreshCompetitions, 
                lastUpdate 
            }) => (
                <Comp
                    {...props}
                    currentDate={currentDate}
                    changeDate={changeDate}
                    tournaments={tournaments}
                    loading={loading}
                    refreshing={refreshing}
                    refreshCompetitions={refreshCompetitions}
                    lastUpdate={lastUpdate}
                />
            )}
        </CalendarContext.Consumer>
    );
}

export default CalendarProvider;
