import React from 'react';
import { View, StyleSheet } from 'react-native';

import { withTheme } from './Providers/ThemeProvider';

const styles = {
    seperatorStyle: {
        height: StyleSheet.hairlineWidth,
    }
};

const Divider = (props) => (
    <View
        {...props}
        style={[
            styles.seperatorStyle,
            props.style, 
            { backgroundColor: props.theme === 'dark' ?
            'rgba(255,255,255,0.25)' : '#485A7F' 
        }]}
    />
);

export default withTheme(Divider);
