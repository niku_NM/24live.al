// @flow
import React from 'react';
import { View } from 'react-native';

import CompetitionName from './CompetitionName';
import Theme from '../Global/Theme';
import { withTheme } from '../Global/Providers/ThemeProvider';
import ActiveMatches from './ActiveMatches';


const styles = {
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 40
  },
  itemsContainer: {
    // width: '16%', 
    flexDirection: 'row-reverse', 
    alignItems: 'center'
  }
};


type Props = {
  flag: string;
  name: string;
  theme: 'light' | 'dark';
}

const CompetitionItem = (props: Props) => {
  const backgroundColor = Theme.getBgColor(props.theme, 1);
  return (
    <View style={[styles.container, { backgroundColor }]}>
      {/* <View style={{ width: '84%' }}> */}
      <View style={{ flex: 1 }}>
        <CompetitionName
          flag={props.flag}
          name={props.name}
          onPress={props.onPress}
          theme={props.theme}
          containerStyle={{
            paddingHorizontal: 12,
            paddingVertical: 8,
          }}
        />
        </View>
        <View style={styles.itemsContainer}>
        {/* <StarIcon 
          initialIsFavorite={props.initialIsFavorite} 
          isFavorite={props.isFavorite} 
          competitionId={props.name} 
        /> */}
        <ActiveMatches active={props.active} />
        </View>
      </View>
   
  );
};

CompetitionItem.defaultProps = {
  theme: 'dark',
};


export default withTheme(CompetitionItem);
