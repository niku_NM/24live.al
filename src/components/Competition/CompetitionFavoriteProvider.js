// @flow
import * as React from 'react';
import Favorite from '../../utils/Favorite';

type Action = 'add' | 'remove';

const defaultProps = {
    favorites: {},
    data: [],
    competitions: [],
    modifyFavorite: () => {},
    loadCompetitions: () => {},
};

const FavoriteContext = React.createContext(defaultProps);

class CompetitionFavoriteProvider extends React.Component<any, any> {
    constructor(props) {
      super(props);
      this.state = {
        favorites: {},
        data: [],
        competitions: [],
        modifyFavorite: this.modifyFavorite,
        loadCompetitions: this.loadCompetitions,
      };
    }

    componentDidMount() {
        Favorite.getCompetition()
            .then(favorites => {
                this.setState(prevState => ({
                    favorites,
                    data: prevState.competitions.filter(item => !!favorites[item.id])
                }));
            });
    }
  
    modifyFavorite = (id, action: Action) => {
        if (action === 'add') {
            Favorite.saveCompetition(id)
            .then(success => {
                if (!success) { return; }
                this.updateFavoriteData(id, true);
            });
        }

        if (action === 'remove') {
            Favorite.removeCompetition(id)
                .then(success => {
                    if (!success) { return; }
                    this.updateFavoriteData(id, false);
                });
        }
    }

    loadCompetitions = (data = []) => {
        this.setState(prevState => ({
            competitions: data,
            data: data.filter(item => !!prevState.favorites[item.id])
        }));
    }

    updateFavoriteData = (id, value) => {
        this.setState(prevState => {
            const newData = { ...prevState.favorites, [id]: value };
            return {
                favorites: newData,
                data: this.state.competitions.filter(item => !!newData[item.id])
            };
        });
    }
  
    render() {
      return (
        <FavoriteContext.Provider value={this.state}>
          {this.props.children}
        </FavoriteContext.Provider>
      );
    }
}

export function withFavorites(Comp: React.ComponentType<any>) {
    return (props: any) => (
      <FavoriteContext.Consumer>
        {({ loadCompetitions, modifyFavorite, data, favorites }) => (
          <Comp 
            {...props} 
            favorites={data}
            favoritesMap={favorites}
            loadCompetitions={loadCompetitions} 
            modifyFavorite={modifyFavorite} 
          />
        )}
      </FavoriteContext.Consumer>
    );
  }
  

export default CompetitionFavoriteProvider;
  
