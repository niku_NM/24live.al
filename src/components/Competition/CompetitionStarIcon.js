import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import ButtonIcon from '../Global/Button/ButtonIcon';
import { withFavorites } from '../Global/FavProviderNew';
import { withTheme } from '../Global/Providers/ThemeProvider';

class StarIcon extends React.Component {
    state = { active: false, hasChange: false };
   
    setIconActive(active) {
        this.setState({ active, hasChange: true, });
    }

    toggleIcon = () => {
        if (this.props.favorites[this.props.matchId]) {
            this.props.modifyFavorite(this.props.matchId, 'remove');
            return;
        }
        this.props.modifyFavorite(this.props.matchId, 'add');
    }

    get icon() {
        if (this.props.favorites[this.props.matchId]) {
            return 'star';
        }

        return 'star-border';
    }

    render() {
        return (
            <React.Fragment>
                {this.props.loading === this.props.matchId ?
                    <View style={{ width: 36 }}>
                        <ActivityIndicator size={24} color='white' />
                    </View> : 
                    <ButtonIcon icon={this.icon} flat onPress={this.toggleIcon} />
                }
             </React.Fragment>
        );
    }
}

export default withTheme(withFavorites(StarIcon));
