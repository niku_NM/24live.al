import React from 'react';
import { View, } from 'react-native';

import Text from '../../Global/Text';
import { withTheme } from '../../Global/Providers/ThemeProvider';
import Theme from '../../Global/Theme';
import Icon from '../../Global/Icon';
import { TouchableItem } from '../../Global/Button';

const StandingsButton = ({ onPress, theme }) => {
    const backgroundColor = Theme.getBgColor(theme, 1);
    const color = Theme.getTextColor(theme);
    const grey = Theme.getTextColor(theme, 1);

    return (
        <TouchableItem
            onPress={onPress}
        >
        <View style={[styles.container, { backgroundColor }]}>
            <Icon 
                name='format-list-numbered' 
                color={grey} 
                size={24} 
                style={{ paddingHorizontal: 6 }} 
            />
            <Text style={{ color, fontWeight: '500', fontSize: 16, paddingHorizontal: 6 }}>
                Shko tek klasifikimet
            </Text>
            <View style={{ alignItems: 'flex-end', flex: 1, }}>
                <Icon 
                    name='keyboard-arrow-right' 
                    color={grey} 
                    size={24} 
                />
            </View>
        </View>
        </TouchableItem>
    );
};

const styles = {
    container: {
        flexDirection: 'row', 
        height: 38, 
        alignItems: 'center', 
        borderWidth: 0.5, 
        elevation: 4, 
        marginHorizontal: 6,
        marginVertical: 10
    }
};

export default withTheme(StandingsButton);
