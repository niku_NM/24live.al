import React from 'react';
import { View } from 'react-native';
import FastImage from 'react-native-fast-image';

import { withTheme } from '../../Global/Providers/ThemeProvider';
import IconInfo from '../../Global/IconInfo';


const SingleTeam = ({ imageUrl, team, theme, direction = 'ltr' }) => (
        <View style={{ width: '42%' }}>
            <IconInfo
                icon={
                    <FastImage
                        source={{ 
                            uri: imageUrl, 
                            priority: FastImage.priority.high, 
                        }}
                        style={styles.img}
                        resizeMode={FastImage.resizeMode.cover}
                    />
                }
                title={team}
                titleProps={{ style: { fontSize: 12, paddingHorizontal: 2 } }}
                padding={4}
                theme={theme}
                direction={direction}
            />
        </View>
);

const styles = {
    img: {
        width: 20,
        height: 20,
        // resizeMode: 'cover',
    },
};

export default withTheme(SingleTeam);
