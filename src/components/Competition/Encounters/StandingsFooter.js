import React from 'react';
import { View, } from 'react-native';

import Text from '../../Global/Text';
import Promotion from '../../LiveScoreComp/MatchDetails/Standings/Promotion';
import CompetitionListWraper from '../CompetitionListWraper';
import Theme from '../../Global/Theme';
import { withTheme } from '../../Global/Providers/ThemeProvider';


function formatPromotions(promotionsObj) {
    return Object.keys(promotionsObj).map(key => promotionsObj[key]);
}

class StandingsFooter extends React.Component {
    static getDerivedStateFromProps(nextProps) {
        const { data } = nextProps;
        if (data && data.standingsTables && data.standingsTables[0]) {
            return {
                promotions: formatPromotions(data.standingsTables[0].promotions)
            };
        }
        return null;
    }

    state = { promotions: [], }


    render() {
        const color = Theme.getTextColor(this.props.theme, 1);
        const backgroundColor = Theme.getBgColor(this.props.theme, 3);
        return (
            <React.Fragment>
            {this.state.promotions.length !== 0 ? 
            <CompetitionListWraper style={{ backgroundColor, }}>
                {this.state.promotions.map(item => (
                    <View style={styles.containerStyle} key={item.id}>
                        <View style={{ width: 20, marginRight: 4 }}>
                        <Promotion id={item.id} />
                        </View>
                   
                        <Text numberOfLines={1} style={{ color, flex: 1 }}>{item.name}</Text>
                        
                    </View>
                ))}
            </CompetitionListWraper>
            : null
            }
            </React.Fragment>
        );
    }
}

const styles = {
    containerStyle: {
        flexDirection: 'row',
        margin: 6,
    },
};

export default withTheme(StandingsFooter);
