import React from 'react';
import { FlatList, ScrollView, View, ActivityIndicator, } from 'react-native';

import CompetitionListWraper from '../CompetitionListWraper';
import { withTheme } from '../../Global/Providers/ThemeProvider';
import Divider from '../../Global/Divider';
import TableHeader from '../../LiveScoreComp/MatchDetails/Standings/TableHeader';
import SingleRow from '../../LiveScoreComp/MatchDetails/Standings/SingleRow';
import services from '../../../services';
import Title from '../../Global/Title';
import Header from '../../Global/Header/Header';
import Theme from '../../Global/Theme';
import Text from '../../Global/Text';
import StandingsFooter from './StandingsFooter';


class Standings extends React.Component {
    state = {
        standingsData: {},
        table: [],
        loading: false,
        title: ''
    };

    componentDidMount() {
        const data = this.props.navigation.getParam('standings', {});
        this.setState({ loading: true, });
        const tournamentId = data.tournament.id;
        const seasonId = data.season.id;
        const title = data.season.name;
        this.setState({ title });
        services.getStandings(tournamentId, seasonId)
            .then(standingsData => {
                this.setState({
                    loading: false,
                    standingsData: standingsData || {},
                    table: standingsData.standingsTables[0] ? 
                    standingsData.standingsTables[0].tableRows : []
                });
            })
            .catch(error => console.log(error));
    }

    renderItem = ({ item }) => <SingleRow showPromotion {...item} />;

    renderEmpty() {
		if (this.state.loading) {
			return (
				<View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
					<ActivityIndicator size={40} color="red" />
					<Text style={{ color: 'white', fontWeight: 'bold' }}>loading</Text>
				</View>
			);
		}

		return (
			<View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
				<Text style={{ fontWeight: 'bold', color: 'red' }} >LIST IS EMPTY</Text>
			</View>
		);
	}

    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: Theme.getBgColor(this.props.theme, 1) }}>
                <Title title={this.state.title} />
                <CompetitionListWraper>
                    {/* <TableHeader /> */}
                    <FlatList
                        data={this.state.table || []}
                        renderItem={this.renderItem}
                        ListHeaderComponent={() => <TableHeader />}
                        ItemSeparatorComponent={() => <Divider />}
                        keyExtractor={(item, index) => `${item.id || index}`}
                        ListEmptyComponent={() => this.renderEmpty()}
                    />
                </CompetitionListWraper>
                <StandingsFooter data={this.state.standingsData} />
            </ScrollView>
        );
    }
}

const StandingsWithTheme = withTheme(Standings);

StandingsWithTheme.navigationOptions = () => {
    return {
      title: 'Klasifikimi',
      header: Header,
      headerRight: (
          <View style={{ width: 30 }} />
      )
    };
  };

  export default StandingsWithTheme;

