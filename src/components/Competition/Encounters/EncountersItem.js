// @flow
import React from 'react';
import { View, } from 'react-native';
import LottieView from 'lottie-react-native';

import Text from '../../Global/Text';
import Theme from '../../Global/Theme';
import { withTheme } from '../../Global/Providers/ThemeProvider';
import TouchableItem from '../../Global/Button/TouchableItem';
import SingleTeam from './SingleTeam';
import CompetitionStarIcon from '../../Competition/CompetitionStarIcon';

type Props = {
    homeTeam: string;
    homeTeamFlag: string;
    awayTeam: string;
    awayTeamFlag: string;
    startTime: number | string;
    homeScore: number;
    awayScore: number;
    homeScoreP1: number;
    awayScoreP1: number;
    homeScoreCurrent: number;
    awayScoreCurrent: number;
    showDate: boolean;
    hideStar: boolean;
    matchId: number;
    status: 'notstarted' | 'inprogress' | 'finished' | 'canceled' | 'postponed';
    statusDescription: string;
    onPress: Function;
    theme: 'light' | 'dark';
}

const styles = {
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 52,
        paddingHorizontal: 8,
        flex: 1
    },
    score: {
        width: '16%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 2
    },
    starStyle: {
        alignItems: 'center',
        justifyContent: 'center',
    }
};

class Encounters extends React.Component<Props, {}> {
    static defaultProps = {
        theme: 'dark'
    }

    renderNotStarted() {
        const {
            startTime,
            theme,
        } = this.props;

        return (
            <View>
                <Text
                    style={{ 
                        color: Theme.getPrimaryColor(theme), 
                        fontSize: 12, 
                        textAlign: 'center' 
                    }}
                >
                    {startTime}
                </Text>
                <Text
                    style={{ color: Theme.getTextColor(theme), fontSize: 10, textAlign: 'center' }}
                >
                    VS
                </Text>
            </View>
        );
    }

    renderInProgress() {
        const {
            homeScoreCurrent,
            awayScoreCurrent,
            theme,
            statusDescription
        } = this.props;

        const color = Theme.getPrimaryColor(theme);
        const textColor = Theme.getTextColor(theme, 2);

        return (
            <View>
                <Text
                    style={{ color, fontSize: 16, textAlign: 'center', letterSpacing: 2 }}
                >
                    {homeScoreCurrent}-{awayScoreCurrent}
                </Text>
                <View 
                    style={{ 
                        flexDirection: 'row', 
                        justifyContent: 'center', 
                        alignItems: 'center' }}
                >
                    <LottieView
                        source={require('../../../assets/pulse_effect.json')}
                        autoPlay
                        loop
                        style={{ height: 12, width: 12, }}
                    />
                    <Text
                        style={{ color: textColor, fontSize: 10, textAlign: 'center', }}
                    >{statusDescription}'</Text>
                </View>
            </View>
        );
    }

    renderFinished() {
        const {
            homeScore,
            awayScore,
            theme,
            statusDescription
        } = this.props;

        const color = Theme.getPrimaryColor(theme);
        const textColor = Theme.getTextColor(theme, 2);
        return (
            <View>
                <Text style={{ color, fontSize: 16, textAlign: 'center' }}>
                    {homeScore} - {awayScore}
                </Text>
                <Text style={{ color: textColor, fontSize: 10, textAlign: 'center' }}>
                    {statusDescription}
                </Text>
            </View>
        );
    }

    renderCanceled() {
        const textColor = Theme.getTextColor(this.props.theme, 2);
        return (
            <View>
                <Text style={{ color: textColor, fontSize: 10, textAlign: 'center' }}>
                    Anulluar
                </Text>
            </View>
        );
    }

    renderPostponed() {
        const textColor = Theme.getTextColor(this.props.theme, 2);
        return (
            <View>
                <Text style={{ color: textColor, fontSize: 10, textAlign: 'center' }}>
                    Shtyre
                </Text>
            </View>
        );
    }

    renderResult() {
        switch (this.props.status) {
            case 'notstarted':
                return this.renderNotStarted();
            case 'inprogress':
                return this.renderInProgress();
            case 'finished':
                return this.renderFinished();
            case 'canceled':
                return this.renderCanceled();
                case 'postponed':
                return this.renderPostponed();
            default:
                return null;
        }
    }

    render() {
        const {
            homeTeam,
            homeTeamFlag,
            awayTeam,
            awayTeamFlag,
            onPress,
            showDate,
            theme,
            matchId,
            hideStar
        } = this.props;

        const textColor = Theme.getTextColor(theme, 2);
        const backgroundColor = Theme.getBgColor(theme, 1);

        return (
            <View style={{ flexDirection: 'row' }}>
            <TouchableItem
                onPress={onPress}
                pressColor={Theme.getButtonPressColor(theme)}
                style={{ flexDirection: 'row', flex: 1 }}
            >
                <View style={[styles.container, { backgroundColor }]}>
                    <SingleTeam
                        imageUrl={homeTeamFlag}
                        theme={theme}
                        team={homeTeam}
                    />
                    <View style={[styles.score, { backgroundColor: Theme.getBgColor(theme, 3) }]}>
                        {showDate ?
                            <Text
                                numberOfLines={1}
                                style={{ color: textColor, textAlign: 'center', fontSize: 9 }}
                            >
                                {showDate}
                            </Text> : null}
                        {this.renderResult()}
                    </View>
                    <SingleTeam
                        imageUrl={awayTeamFlag}
                        theme={theme}
                        team={awayTeam}
                        direction="rtl"
                    />
                </View>
            </TouchableItem>
            {hideStar ? null :
                    <View 
                        style={[
                            styles.starStyle, 
                            { backgroundColor: Theme.getBgColor(theme, 3) }
                            ]}
                    >
                        <CompetitionStarIcon matchId={matchId} />
                    </View>
                }
            </View>
        );
    }
}

export default withTheme(Encounters);
