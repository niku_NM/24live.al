// @flow
import React from 'react';
import { View } from 'react-native';

import Text from '../Global/Text';
import Theme, {} from '../Global/Theme';
import { withTheme } from '../Global/Providers/ThemeProvider';

const styles = {
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        marginHorizontal: 4,
        overflow: 'hidden'
    },
    defaultTitleStyle: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 6
    }
};

type Props = {
    active: number;
    theme: 'dark' | 'light';
    containerStyle: Object;
    style: Object;
}

class ActiveMatches extends React.Component<Props, {}> {
    static defaultProps = {
        titleProps: {},
        theme: 'dark',
    }

    renderTitle() {
        const { active, theme } = this.props;
         const backgroundColor = Theme.getBgColor(theme);
         const color = Theme.getPrimaryColor(theme);
        return (
            <View 
                style={styles.container}
            >
                <Text
                    style={[styles.defaultTitleStyle, { backgroundColor }, { color }]}
                >
                    {active}
                </Text>
            </View> 
        );
    }

    render() {
        return (
            <View>
                {this.renderTitle()}
            </View>
        );
    }
}

export default withTheme(ActiveMatches);
