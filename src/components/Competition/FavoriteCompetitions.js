import React from 'react';
import { View } from 'react-native';
import { withNavigation } from 'react-navigation';

import CompetitionListWraper from './CompetitionListWraper';
import CompetitionItem from './CompetitionItem';
import Divider from '../Global/Divider';
import IconInfo from '../Global/IconInfo';
import { withFavorites } from './FavoriteProvider';
import { withTheme } from '../Global/Providers/ThemeProvider';

class FavoriteCompetitions extends React.Component {
    render() {
        if (!this.props.favorites.length) {
            return null;
        }
        return (
            <React.Fragment>
                <IconInfo
                    icon="star"
                    title="Favorites Championships"
                    iconProps={{ size: 24 }}
                    containerStyle={{ padding: 8 }}
                    theme={this.props.theme}
                />
                <CompetitionListWraper>
                    {this.props.favorites.map((item, index) => (
                        <View key={index}>
                            <CompetitionItem
                                onNamePress={() => this.props.navigation.navigate('MatchDetails')}
                                name={item.name}
                                flag={item.flag}
                                theme={this.props.theme}
                                active={item.active}
                                isFavorite
                            />
                            { this.props.favorites.length - 1 !== index ? <Divider /> : null}
                        </View>
                    )
                    )}
                </CompetitionListWraper>
            </React.Fragment>

        );
    }
}

const FavoriteCompetitionsWithTheme = withFavorites(withTheme(FavoriteCompetitions));

export default withNavigation(FavoriteCompetitionsWithTheme);
