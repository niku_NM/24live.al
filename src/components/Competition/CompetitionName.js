// @flow
import React from 'react';
import FastImage from 'react-native-fast-image';

import IconInfo from '../Global/IconInfo';

import TouchableItem from '../Global/Button/TouchableItem';

import Theme from '../Global/Theme';

type Props = {
  flag: string;
  name: string;
  onPress: Function;
  theme: 'light' | 'dark';
}

const CompetitionName = (props: Props) => {
  const { flag, name, onPress, ...rest } = props;
  return (
    <TouchableItem
      onPress={onPress}
      pressColor={Theme.getButtonPressColor(rest.theme)}
    >
      <IconInfo
        icon={
          <FastImage
            source={{ uri: flag }}
            style={{ width: 24, height: 24, }}
            resizeMode={FastImage.resizeMode.contain}
          />
        }
        title={name}
        {...rest}
      />
    </TouchableItem>
  );
};

CompetitionName.defaultProps = {
  onPress: () => { },
};

export default CompetitionName;
