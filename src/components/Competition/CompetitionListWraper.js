import React from 'react';
import { View } from 'react-native';
import { withTheme } from '../Global/Providers/ThemeProvider';
import Theme from '../Global/Theme';

const CompetitionListWraper = (props) => (
    <View
        {...props}
        style={[{
            borderRadius: 8,
            margin: 8,
            borderWidth: 1,
            borderColor: Theme.getBgColor(props.theme),
            overflow: 'hidden',
            elevation: 4,
            backgroundColor: Theme.getBgColor(props.theme, 1),
        }, props.style]}
    />
);

export default withTheme(CompetitionListWraper);
