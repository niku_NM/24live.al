import React from 'react';
import { View } from 'react-native';

import { TouchableItem } from '../Global/Button';
import Text from '../Global/Text';
import { withTheme } from '../Global/Providers/ThemeProvider';
import Theme from '../Global/Theme';
import Icon from '../Global/Icon';

const AllGamesButton = ({ onPress, theme }) => {
    const backgroundColor = Theme.getBgColor(theme, 3);
    const color = Theme.getPrimaryColor(theme);
    const white = Theme.getTextColor(theme);

    return (
        <TouchableItem onPress={onPress}>
            <View style={[styles.containerStyle, { backgroundColor, borderBottomColor: white }]}>
                <Text style={{ color, fontSize: 16, fontWeight: 'bold' }}>
                    SHIKO TE GJITHA
                </Text>
                <View style={{ alignItems: 'flex-end', flex: 1, }}>
                <Icon name='keyboard-arrow-right' size={30} color={white} />
                </View>
            </View>
        </TouchableItem>
    );
};

const styles = {
    containerStyle: {
        height: 50, 
        borderBottomWidth: 1,
        alignItems: 'center',
        paddingHorizontal: 10,
        flexDirection: 'row'
    },
};

export default withTheme(AllGamesButton);
