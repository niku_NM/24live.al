import moment from 'moment';
import firebase, { RNFirebase } from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';
import md5 from 'md5';

async function registerNotificationToken(uid: string, active = true, defaultToken = null) {
    try {
      // const deviceUniqueId = await DeviceInfo.getUniqueID()
      const brand = DeviceInfo.getBrand();
      const model = DeviceInfo.getModel();
      const systemVersion = DeviceInfo.getSystemVersion();
      const carrier = DeviceInfo.getCarrier();
      const deviceCountry = DeviceInfo.getDeviceCountry();
      const deviceId = DeviceInfo.getDeviceId();
      const manufacturer = DeviceInfo.getManufacturer();
      const timezone = DeviceInfo.getTimezone();
      const version = DeviceInfo.getVersion();
      const isTablet = DeviceInfo.isTablet();
  
      let fcmToken = defaultToken;
      if (!defaultToken) {
        fcmToken = await firebase.messaging().getToken();
      }
  
      if (!fcmToken) { return; }
      const phoneTokenId = md5(model);
      await firebase.firestore().doc(`users/${uid}/phoneTokens/${phoneTokenId}`).set({
        token: fcmToken,
        createdAt: moment.utc().toDate(),
        active,
        device: {
          brand,
          model,
          systemVersion,
          carrier,
          deviceCountry,
          deviceId,
          manufacturer,
          timezone,
          version,
          isTablet,
        },
      }, { merge: true });
      // console.log('TOKEN REGISTER');
    } catch (error) {
      console.log(error);
    }
  }

export default {
    registerNotificationToken
}