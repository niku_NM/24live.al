import axios from 'axios';
import moment from 'moment';

const BASE_URL = 'https://www.sofascore.com';
const tournamentLogoUrl = (id) => `${BASE_URL}/u-tournament/${id}/logo`;
const teamLogoUrl = (id) => `${BASE_URL}/images/team-logo/football_${id}.png`;
const playerLogoUrl = (id) => `${BASE_URL}/images/player/image_${id}.png`;
const noPlayerLogoUrl = () => 
    `${BASE_URL}/bundles/sofascoreweb/images/img_empty_state/no_player_img.png`;


function getCompetitions(date = undefined) {
    return axios
        .get(`${BASE_URL}/football//${moment(date).format('YYYY-MM-DD')}/json?_=${moment().unix()}`)
        .then(res => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log(error);
            return {};
        });
}

function getLiveMatches() {
    return axios
        .get(`${BASE_URL}/football/livescore/json?=${moment().unix()}`)
        .then(res => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log(error);
            return {};
        });
}

function getMatchData(id) {
    return axios
        .get(`${BASE_URL}/event/${id}/json?_=${moment().unix()}`)
        .then(res => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log(error);
            return {};
        });
}

function getLastGames(id) {
    const url = `${BASE_URL}/event/${id}/matches/json?_=${moment().unix()}`;
    console.log(url, 'url here');
    return axios
        .get(url)
        .then(res => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log(error);
            return {};
        });
}

function getLineUps(id) {
    return axios
        .get(`${BASE_URL}/event/${id}/lineups/json?_=${moment().unix()}`)
        .then(res => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log(error);
            return {};
        });
}

function getStandings(tournamentId, seasonId) {
    return axios
    .get(`${BASE_URL}/tournament/${tournamentId}/${seasonId}/standings/tables/json?_=${moment().unix()}`)
    .then(res => {
        console.log(res);
        return res.data;
    })
    .catch(error => {
        console.log(error);
        return {};
    });
}

function getNews(page) {
    return axios
        .get(`http://lottosportsystem.com/wp-json/wp/v2/posts?_embed&categories=1&page=${page}`)
        .then(res => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log(error);
            return {};
        });
}

export default {
    getCompetitions,
    getLiveMatches,
    tournamentLogoUrl,
    teamLogoUrl,
    playerLogoUrl,
    noPlayerLogoUrl,
    getMatchData,
    getLastGames,
    getLineUps,
    getStandings,
    getNews,
};
