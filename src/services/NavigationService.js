import { NavigationActions } from 'react-navigation';

// eslint-disable-next-line
let _navigator

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  if (!_navigator) { return; }
  _navigator.dispatch(NavigationActions.navigate({
    routeName,
    params,
  }));
}

function getCurrentRoute(state = null) {
  const findCurrentRoute = (navState) => {
    if (navState.index !== undefined) {
      return findCurrentRoute(navState.routes[navState.index])
    }
    return navState;
  };

  if (!state && !_navigator) {
    return {};
  }

  return findCurrentRoute(state || _navigator.state.nav);
}

// add other navigation functions that you need and export them

export default {
  navigate,
  setTopLevelNavigator,
  getCurrentRoute,
};

