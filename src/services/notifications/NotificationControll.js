// @flow
import { Vibration, Platform } from 'react-native';
import firebase, {
    type Notification,
    type NotificationOpen,
    type RemoteMessage
} from 'react-native-firebase';

import NavigationService from '../NavigationService';


class NotificationService {

  async subscribe() {
    const hasPermission = await this.requestPermissionIfNeeded();
    if (!hasPermission) { return; }
    console.log(hasPermission, 'NotificationsScreen.subscribe');
    this.notificationDisplayedListener = firebase.notifications()
      .onNotificationDisplayed((notification: Notification) => {
        // Process your notification as required
        // ANDROID: Remote notifications do not contain the channel ID.
        // You will have to specify this manually if you'd like to re-display the notification.
        console.log(notification, 'DISPLAY');
      });
    this.notificationListener = firebase.notifications()
      .onNotification((notification: Notification) => {
        // Process your notification as required
        this.showNotification(notification);
        console.log(notification, 'notificationListener');
      });
    this.notificationOpenedListener = firebase.notifications()
      .onNotificationOpened((notificationOpen: NotificationOpen) => {
        // Get the action triggered by the notification being opened
        const { action } = notificationOpen;
        // Get information about the notification that was opened
        const { notification }: { notification: Notification } = notificationOpen;
        this.handleNotification(notification);
        console.log(notification, action, 'notificationOpenedListener');
      });

    this.messageListener = firebase.messaging()
      .onMessage((message: RemoteMessage) => {
        // Process your message as required
        console.log(message, 'messageListener');
      });

    const notificationOpen: NotificationOpen = await firebase.notifications()
      .getInitialNotification();
    console.log(notificationOpen, '____');
    if (notificationOpen) {
      // App was opened by a notification
      // Get the action triggered by the notification being opened
      const { action } = notificationOpen;
      // Get information about the notification that was opened
      const { notification } = notificationOpen;
      this.handleNotification(notification);
      console.log(notification, action, 'getInitialNotification');
    }
  }


  // eslint-disable-next-line
  showNotification(notification: Notification) {

    if (Platform.OS === 'android') {
      const channel = new firebase.notifications.Android.Channel(
        'live24', 'Live24 Channel', firebase.notifications.Android.Importance.Max
      )
        .setDescription('Live24 Channel');
      firebase.notifications().android.createChannel(channel);

      notification.android.setChannelId('live24');
      // notification.setTitle(notification.title); //added
      // notification.setBody(notification.title); //added
      // .android.setSmallIcon('ic_stat_icon_notif').android.setColor('#BF2046');
    }
    firebase.notifications().displayNotification(notification);
    Vibration.vibrate();
  }

  handleNotification(notification: Notification) {
    console.log(notification, 'notificationnik');
    // NavigationService.navigate();
    const { data } = notification;
    let navigationArgs = [];
    switch (data.action) {
        case 'com.newmedia.live24.Match':
            navigationArgs = ['MatchDetails', { id: data.matchId }];
            break;
        default:
          navigationArgs = ['Home'];
        break;
    }
    NavigationService.navigate(...navigationArgs);
  }

  unsubcribe() {
    if (this.notificationDisplayedListener) {
      this.notificationDisplayedListener();
    }
    if (this.notificationListener) {
      this.notificationListener();
    }
    if (this.notificationOpenedListener) {
      this.notificationOpenedListener();
    }
    if (this.messageListener) {
      this.messageListener();
    }
  }

  requestPermissionIfNeeded = async (): Promise<boolean> => {
    try {
      const enabled = await firebase.messaging().hasPermission();
      if (enabled) {
        return true;
      }
      await firebase.messaging().requestPermission();
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
}

export default NotificationService;
