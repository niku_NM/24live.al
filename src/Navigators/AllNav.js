import React from 'react';
import { View, Dimensions, Linking } from 'react-native';
import {
    createDrawerNavigator,
    createBottomTabNavigator,
    createStackNavigator,
} from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';

import DrawerItems from './DrawerItems';
import NewsScreen from '../screens/NewsScreen';
import MatchDetails from '../screens/MatchDetails';
import ThemeToggle from '../components/DrawerComp/ThemeToggle';
import DrawerLogo from '../assets/drawerLogo';
import { withTheme } from '../components/Global/Providers/ThemeProvider';
import Theme from '../components/Global/Theme';
import TabBar from './TabBar';
import HomeScreen from '../screens/HomeScreen/HomeScreen';
import CompetitionMatches from '../screens/HomeScreen/CompetitionMatches';
import OpenedNews from '../components/NewsComp/OpenedNews';
import LiveScreen from '../screens/LiveScreen';
import Standings from '../components/Competition/Encounters/Standings';
import FavoritesScreen from '../screens/FavoritesScreen';
import AllGames from '../screens/HomeScreen/AllGames';
import FavNumBadge from './FavNumBadge';
import CustomDrawerLabel from '../components/DrawerComp/CustomDrawerLabel';


const DRAWER_WIDTH = Dimensions.get('window').width / 1.4;

const TabNav = createBottomTabNavigator({
    Home: {
        screen: createStackNavigator({
            HomeScreen: {
                screen: HomeScreen,
            },
            CompetitionMatches: {
                screen: CompetitionMatches,
            },
            AllGames: {
                screen: AllGames
            },
            Standings: {
                screen: Standings,
            },
        }, {
                headerMode: 'screen',
            }),
        navigationOptions: {
            tabBarLabel: 'Home',
            tabBarIcon: ({ tintColor }) => (
                <Icon name="home" color={tintColor} size={24} />
            )
        }
    },

    Live: {
        screen: createStackNavigator({
            LiveScreen: {
                screen: LiveScreen,
            },
        }, {
                headerMode: 'screen',
            }),
        navigationOptions: {
            tabBarLabel: 'Live',
            tabBarIcon: ({ tintColor }) => (
                <Icon name="live-tv" color={tintColor} size={24} />
            )
        }
    },

    Favourites: {
        screen: createStackNavigator({
            FavoritesScreen: {
                screen: FavoritesScreen
            }
        }, {
            headerMode: 'screen',
        }),
        navigationOptions: {
            tabBarLabel: 'Favoritet',
            tabBarIcon: ({ tintColor }) => (
                <View>
                <Icon name="star" color={tintColor} size={24} />
                <FavNumBadge />
                </View>
            )
        }
    },

    News: {
        screen: createStackNavigator({
            NewsScreen: {
            screen: NewsScreen
        },
        OpenedNews: {
            screen: OpenedNews
        }
        }, {
                headerMode: 'screen',
            }),
        navigationOptions: {
            tabBarLabel: 'Lajmet',
            tabBarIcon: ({ tintColor }) => (
                <Icon name="speaker-notes" color={tintColor} size={24} />
            )
        }
    },
},
    {
        tabBarComponent: TabBar,
        tabBarOptions: {
            style: {
                elevation: 4,
                shadowOpacity: 4,
            }
        }
    }
);

const Stack = createStackNavigator({
    TabNav: {
        screen: TabNav,
        navigationOptions: {
            header: null,
        }
    },
    MatchDetails: {
        screen: MatchDetails
    },
    LiveMatchDetails: {
        screen: MatchDetails
    },
}, {
    mode: 'modal'
}
);

const CustomComponent = withTheme((props) => (
    <View style={{ flex: 1, backgroundColor: Theme.getBgColor(props.theme, 1), }}>
        <View style={{ paddingHorizontal: 16, paddingVertical: 18 }}>
            <DrawerLogo width={140} height={48} fill={Theme.getTextColor(props.theme, 1)} />
        </View>
        <View style={{ flex: 1 }}>
            <DrawerItems {...props} />
            <CustomDrawerLabel 
                iconName='feedback' 
                title='Dergo Feedback'
                onPress={() => Linking.openURL('mailto:lottosport62@gmail.com')}
            />
            <CustomDrawerLabel 
                iconName='star-half' 
                title='Na vleresoni'
                onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=al.newmedia.live24')}
            />
            <ThemeToggle />
        </View>
    </View>
));


const Drawer = createDrawerNavigator({
    Home: {
        screen: Stack,
        navigationOptions: {
            drawerLabel: 'Home',
            drawerIcon: ({ tintColor }) => (
                <Icon name="home" color={tintColor} size={24} />
            )
        }
    },
    // Notifications: {
    //     screen: NotificationStack,
    //     navigationOptions: {
    //         drawerLabel: 'Notifications',
    //         drawerIcon: ({ tintColor }) => (
    //             <Icon name="sms-failed" color={tintColor} size={24} />
    //         )
    //     }
    // },
    // Settings: {
    //     screen: TestScreen,
    //     navigationOptions: {
    //         drawerLabel: 'Settings',
    //         drawerIcon: ({ tintColor }) => (
    //             <Icon name="settings" color={tintColor} size={24} />
    //         )
    //     }
    // },
    // About: {
    //     screen: TestScreen,
    //     navigationOptions: {
    //         drawerLabel: 'About',
    //         drawerIcon: ({ tintColor }) => (
    //             <Icon name="info" color={tintColor} size={24} />
    //         )
    //     }
    // },
}, {
        // initialRouteName: 'Home',
        drawerWidth: DRAWER_WIDTH,
        contentComponent: CustomComponent,
        drawerPosition: 'left',
        // useNativeAnimations: true,
        contentOptions: {
            itemsContainerStyle: {
                paddingVertical: 0,
            },
            iconContainerStyle: {
                opacity: 1,
            },
            itemStyle: {
                height: 56,
            },
        },
    }
);


export default Drawer;
