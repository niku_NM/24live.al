import React from 'react';
import { View } from 'react-native';

import Text from '../components/Global/Text';
import { withFavorites } from '../components/Global/FavProviderNew';
// import Theme from '../components/Global/Theme';
// import { withTheme } from '../components/Global/Providers/ThemeProvider';

const FavNumBadge = (props) => {
    return (
        <React.Fragment>
        {props.matches.length !== 0 ? 
        <View 
            style={{
                width: 12,
                height: 12,
                borderRadius: 6,
                backgroundColor: '#ea3a31',
                position: 'absolute',
                top: 0,
                right: 0,
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
        <Text style={{ fontSize: 10, color: '#D7DDEB' }}>
        {props.matches.length}
        </Text>
        </View> 
        : null }
        </React.Fragment>
    );
};


export default withFavorites(FavNumBadge);
