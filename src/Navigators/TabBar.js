// @flow
import * as React from 'react';
import { BottomTabBar } from 'react-navigation-tabs';
import { withTheme, type ThemeProps } from '../components/Global/Providers/ThemeProvider';
import Theme from '../components/Global/Theme';

type Props = ThemeProps & {
    activeTintColor: string;
    inactiveTintColor: string;
    style: Object;
}

const TabBar = (props: Props) => {
    const activeTintColor = props.activeTintColor || Theme.getTextColor(props.theme);
    const inactiveTintColor = props.inactiveTintColor || Theme.getTextColor(props.theme, 2);

    const style = { ...(props.style || {}) };
    style.backgroundColor = style.backgroundColor || Theme.getBgColor(props.theme, 1);
    const otherProps = {
        activeTintColor,
        inactiveTintColor,
        style,
    };
    return (
        <BottomTabBar {...props} {...otherProps} />
    );
};

export default withTheme(TabBar);
