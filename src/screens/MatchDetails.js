import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import MiddleTabNav from '../components/LiveScoreComp/MatchDetails/MiddleTabNav';
import { withTheme } from '../components/Global/Providers/ThemeProvider';
import Header from '../components/Global/Header/Header';
import Theme from '../components/Global/Theme';
// import StarIcon from '../components/Global/StarIcon';
import services from '../services';
import CompetitionStarIcon from '../components/Competition/CompetitionStarIcon';
import { withFavorites } from '../components/Global/FavProviderNew';


class MatchDetails extends React.Component {
  state = {
    loading: false,
    matchData: {},
    headerData: false,
    predictionData: {},
    incidentsData: [],
    statisticsData: {},
    matchId: {},
  };

  componentDidMount() {
    this.$isMounted = true;
    this.fetchData();
    this.intervalID = setInterval(() => {
      this.refreshMatch();
    }, 30 * 1000);
  }

  componentWillUnmount() {
    this.$isMounted = false;
    clearInterval(this.intervalID);
  }

  fetchData = () => {
    const id = this.props.navigation.getParam('id', {});
    this.setState({ loading: true });
    services.getMatchData(id)
      .then(matchData => {
        this.props.navigation.setParams({ title: matchData.event.name });
        if (this.$isMounted) {
          this.setState({
            matchData,
            loading: false,
            headerData: matchData.event,
            predictionData: matchData.vote,
            incidentsData: matchData.incidents.reverse(),
            statisticsData: matchData.statistics || {},
            matchId: id
          });
        }
      });
  }

  refreshMatch() {
    this.fetchData();
  }


  render() {
    if (!this.state.headerData) {
      return (
        <ActivityIndicator 
          size={40} 
          color='red' 
          style={{ backgroundColor: Theme.getBgColor(this.props.theme, 1), flex: 1 }} 
        />
      );
    }
    return (
      <View
        style={{ flex: 1, backgroundColor: Theme.getBgColor(this.props.theme, 1) }}
      >
        <MiddleTabNav
            headerData={this.state.headerData}
            predictionData={this.state.predictionData}
            incidentsData={this.state.incidentsData}
            statisticsData={this.state.statisticsData}
            matchId={this.state.matchId}
        />
      </View>
    );
  }
}

const MatchDetailsWithTheme = withTheme(withFavorites(MatchDetails));

MatchDetailsWithTheme.navigationOptions = ({ navigation }) => {
  const id = navigation.getParam('id', null);
  const title = navigation.getParam('title', null);
  return {
    title,
    headerTitleStyle: {
      width: '76%'
    },
    headerRight: (
      <View style={{ width: 64, alignItems: 'center' }}>
        <CompetitionStarIcon  
          matchId={id}
          // status={data.status.type}
        />
      </View>
    ),
    header: Header,
    headerStyle: {
      shadowOpacity: 0,
      elevation: 0,
    },
  };
};

export default MatchDetailsWithTheme;
