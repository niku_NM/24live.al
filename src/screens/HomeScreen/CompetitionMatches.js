import React from 'react';
import { 
    View, 
    ScrollView, 
    FlatList, 
    ActivityIndicator,
} from 'react-native';
import moment from 'moment';

import Theme from '../../components/Global/Theme';
import { withTheme } from '../../components/Global/Providers/ThemeProvider';
import StatusBar from '../../components/Global/StatusBar';
import Header from '../../components/Global/Header/Header';
import CompetitionListWraper from '../../components/Competition/CompetitionListWraper';
import Encounters from '../../components/Competition/Encounters/EncountersItem';
// import Title from '../../components/Global/Title';
import services from '../../services';
import Divider from '../../components/Global/Divider';
import Text from '../../components/Global/Text';
import StandingsButton from '../../components/Competition/Encounters/StandingsButton';


class CompetitionMatches extends React.Component {
    state = {
        competitionMatches: [],
        loading: false,
        standingsData: {},
        refreshing: false,
    }

    componentDidMount() {
      this.fetchData();
    }

    onItemPress = (item) => {
        this.props.navigation.navigate('MatchDetails', { id: item.id });
    }

    onStandingsPress = (item) => {
        this.props.navigation.navigate('Standings', { standings: item });
    }

    fetchData = () => {
        this.setState({ loading: true });
        const data = this.props.navigation.getParam('data', {});
        this.setState({ 
            competitionMatches: data.events || [], 
            loading: false, 
            standingsData: data || {},
        });
    }

    renderItem = ({ item }) => {
        const { 
            homeTeam, 
            awayTeam, 
            status, 
            statusDescription, 
            startTimestamp, 
            homeScore, 
            awayScore,
            id
        } = item;

        return (
            <Encounters
                homeTeam={homeTeam.name}
                homeTeamFlag={services.teamLogoUrl(homeTeam.id)}
                awayTeam={awayTeam.name}
                awayTeamFlag={services.teamLogoUrl(awayTeam.id)}
                homeScore={homeScore.normaltime}
                awayScore={awayScore.normaltime}
                homeScoreCurrent={homeScore.current}
                awayScoreCurrent={awayScore.current}
                homeScoreP1={homeScore.period1}
                awayScoreP1={awayScore.period1}
                startTime={moment.unix(startTimestamp).local().format('HH:mm')}
                // startTime={moment.unix(startTimestamp).format('HH:mm')}
                status={status.type}
                statusDescription={statusDescription}
                onPress={() => this.onItemPress(item)}
                matchId={id}
            />
        );
    }
    
    renderEmpty() {
        const green = Theme.getPrimaryColor(this.props.theme);
        const color = Theme.getTextColor(this.props.theme);

		if (this.props.loading) {
			return (
				<View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
					<ActivityIndicator size={40} color={green} />
					<Text style={{ color, fontWeight: 'bold' }} > loading </Text>
				</View>
			);
		}

		return (
			<View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
				<Text style={{ fontWeight: 'bold', color }} >LIST IS EMPTY</Text>
			</View>
		);
	}

    render() {
        const standingsData = this.state.standingsData;
        return (
            <ScrollView
                style={{ flex: 1, backgroundColor: Theme.getBgColor(this.props.theme) }}
            >
                <StatusBar />
                <CompetitionListWraper>
                    <FlatList
                        data={this.state.competitionMatches || []}
                        renderItem={this.renderItem}
                        ItemSeparatorComponent={() => <Divider />}
                        keyExtractor={(item) => item.id.toString()}
                        ListEmptyComponent={() => this.renderEmpty()}
                    />
                </CompetitionListWraper>
                <StandingsButton onPress={() => this.onStandingsPress(standingsData)} />
            </ScrollView>
        );
    }
}

const CompetitionMatchesWithTheme = withTheme(CompetitionMatches);

CompetitionMatchesWithTheme.navigationOptions = ({ navigation }) => {
    const competitionTitle = navigation.getParam('data', []);
    return {
    title: competitionTitle.tournament.name,
    header: Header,
    headerRight: (
        <View style={{ width: 64 }} />
    )
 };
};


export default CompetitionMatchesWithTheme;

