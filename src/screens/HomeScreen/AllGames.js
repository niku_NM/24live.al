import React from 'react';
import { 
    View, 
    SectionList, 
    ActivityIndicator, 
    RefreshControl,
    StyleSheet,
    Image 
} from 'react-native';
import moment from 'moment';

import Text from '../../components/Global/Text';
import IconInfo from '../../components/Global/IconInfo';
import Theme from '../../components/Global/Theme';
import { withTheme } from '../../components/Global/Providers/ThemeProvider';
import StatusBar from '../../components/Global/StatusBar';
import Header from '../../components/Global/Header/Header';
import Divider from '../../components/Global/Divider';
import services from '../../services';
import EncountersItem from '../../components/Competition/Encounters/EncountersItem';
import { withDateUpdated } from '../../components/Global/Providers/CalendarProvider';
import { withFavorites } from '../../components/Global/FavProviderNew';


class AllGames extends React.PureComponent {
    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState.lastUpdate === nextProps.lastUpdate) {
            return null;
        }
        return {
            lastUpdate: nextProps.lastUpdate,
            allGames: nextProps.tournaments.map(item => {
                const title = item;
                const data = item.events;
                return {
                    title,
                    data
                };
            })
        };
    }
    state = {
        allGames: [],
        lastUpdate: null,
    }

    
    onItemPress = (item) => {
        this.props.navigation.navigate('LiveMatchDetails', { id: item.id });
    }

    onRefresh = () => {
        this.props.refreshCompetitions();
    }

    refreshControl() {
        return (
          <RefreshControl
            refreshing={this.props.refreshing}
            onRefresh={this.onRefresh} 
          />
        );
    }

    renderItem = ({ item }) => {
        const {
            homeTeam,
            awayTeam,
            status,
            statusDescription,
            startTimestamp,
            homeScore,
            awayScore,
            id
        } = item;

        return (
                <EncountersItem
                    homeTeam={homeTeam.name}
                    homeTeamFlag={services.teamLogoUrl(homeTeam.id)}
                    awayTeam={awayTeam.name}
                    awayTeamFlag={services.teamLogoUrl(awayTeam.id)}
                    homeScore={homeScore.normaltime}
                    awayScore={awayScore.normaltime}
                    homeScoreCurrent={homeScore.current}
                    awayScoreCurrent={awayScore.current}
                    homeScoreP1={homeScore.period1}
                    awayScoreP1={awayScore.period1}
                    startTime={moment.unix(startTimestamp).local().format('HH:mm')}
                    status={status.type}
                    statusDescription={statusDescription}
                    onPress={() => this.onItemPress(item)}
                    matchId={id}
                />
        );
    }

    renderSectionHeader = ({ section }) => {
        const { tournament, category } = section.title;
        // const { theme } = this.props;
        // const borderBottomColor = Theme.getTextColor(theme, 3);
        // const color = Theme.getTextColor(this.props.theme);

        return (
            <View 
                style={[
                    styles.headerContainer, 
                    { 
                        // borderBottomColor, 
                        backgroundColor: Theme.getBgColor(this.props.theme, 2) 
                    }
                ]}
            >
                <IconInfo 
                    icon={
                        <Image
                            source={{ uri: services.tournamentLogoUrl(tournament.uniqueId) }}
                            style={{ width: 24, height: 24, resizeMode: 'contain' }}
                        />
                    }
                    title={`${category.name} / ${tournament.name}`}
                    theme={this.props.theme}
                />
            </View>
        );
    }


    renderEmpty() {
        const green = Theme.getPrimaryColor(this.props.theme);
        const color = Theme.getTextColor(this.props.theme);

		if (this.props.loading) {
			return (
				<View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
					<ActivityIndicator size={40} color={green} />
					<Text style={{ color, fontWeight: 'bold' }} > loading </Text>
				</View>
			);
		}

		return (
			<View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
				<Text style={{ fontWeight: 'bold', color }} >LIST IS EMPTY</Text>
			</View>
		);
	}

    render() {
        return (
            <View
                style={{ flex: 1, backgroundColor: Theme.getBgColor(this.props.theme) }}
            >
                <StatusBar />
                    {this.props.loading && this.renderEmpty()}
                    {!this.props.loading &&
                        <SectionList
                            stickySectionHeadersEnabled
                            style={{
                                flex: 1,
                                backgroundColor: Theme.getBgColor(this.props.theme),
                            }}
                            extraData={this.props.loading}
                            ItemSeparatorComponent={() => <Divider />}
                            sections={this.state.allGames}
                            renderSectionHeader={this.renderSectionHeader}
                            renderItem={this.renderItem}
                            keyExtractor={(item) => item.id.toString()}
                            ListEmptyComponent={() => this.renderEmpty()}
                            refreshControl={this.refreshControl()}
                            initialNumToRender={50}
                            maxToRenderPerBatch={50}
                            windowSize={50}
                        />
                    }
            </View>
        );
    }
}

const styles = {
    headerContainer: {
        overflow: 'hidden',
        paddingHorizontal: 8,
        paddingVertical: 10,
        borderBottomWidth: StyleSheet.hairlineWidth,
    },

};

const AllGamesWithTheme = withFavorites(withTheme(withDateUpdated(AllGames)));

AllGamesWithTheme.navigationOptions = () => ({
    title: 'Ndeshjet e dites',
    header: Header,
    headerRight: (
        <View style={{ width: 64 }} />
    ),
});


export default AllGamesWithTheme;
