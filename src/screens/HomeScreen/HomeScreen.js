import React from 'react';
import {
    View,
    FlatList,
    ActivityIndicator,
    RefreshControl,
} from 'react-native';

import Text from '../../components/Global/Text';
import ShowCalendar from '../../components/LiveScoreComp/ShowCalendar';
import IconInfo from '../../components/Global/IconInfo';
import Competition from '../../components/Competition';
import Theme from '../../components/Global/Theme';
import { withTheme } from '../../components/Global/Providers/ThemeProvider';
import StatusBar from '../../components/Global/StatusBar';
import Header from '../../components/Global/Header/Header';
import ButtonIcon from '../../components/Global/Button/ButtonIcon';
import CompetitionListWraper from '../../components/Competition/CompetitionListWraper';
import Divider from '../../components/Global/Divider';
import Goal from '../../components/LiveScoreComp/MatchDetails/Events/Icons/Goal';
import services from '../../services';
import { withDateUpdated } from '../../components/Global/Providers/CalendarProvider';
import AllGamesButton from '../../components/Competition/AllGamesButton';


class HomeScreen extends React.PureComponent {
    state = {}

    onRefresh = () => {
        this.props.refreshCompetitions();
    }

    onItemPress = (item) => {
        this.props.navigation.navigate('CompetitionMatches', { data: item });
    }

    getItemLayout = (data, index) => (
        { length: 40, offset: 40 * index, index }
    );

    refreshControl() {
        return (
            <RefreshControl
                refreshing={this.props.refreshing}
                onRefresh={this.onRefresh}
            />
        );
    }

    renderItem = ({ item }) => {
        const { tournament, events, category } = item;
        return (
            <Competition.Item
                onPress={() => this.onItemPress(item)}
                name={`${category.name} - ${tournament.name}`}
                flag={services.tournamentLogoUrl(tournament.uniqueId)}
                theme={this.props.theme}
                active={events.length}
            />
        );
    }

    renderEmpty() {
        const green = Theme.getPrimaryColor(this.props.theme);
        const color = Theme.getTextColor(this.props.theme);

        if (this.props.loading) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
                    <ActivityIndicator size={40} color={green} />
                    <Text style={{ color, fontWeight: 'bold' }} > loading </Text>
                </View>
            );
        }

        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
                <Text style={{ fontWeight: 'bold', color }} >LIST IS EMPTY</Text>
            </View>
        );
    }

    render() {
        const borderBottomColor = Theme.getTextColor(this.props.theme, 2);
        return (
            <View
                style={{ flex: 1, backgroundColor: Theme.getBgColor(this.props.theme) }}
            >
                <StatusBar />
                <AllGamesButton 
                    onPress={() => this.props.navigation.navigate('AllGames')} 
                />
                <IconInfo
                    icon={<Goal size={20} />}
                    title="KAMPIONATET"
                    titleProps={{ style: { fontSize: 14 } }}
                    containerStyle={[styles.containerStyle, { borderBottomColor }]}
                    theme={this.props.theme}
                />
                <CompetitionListWraper style={{ flex: 1 }}>
                    {this.props.loading && this.renderEmpty()}
                    {!this.props.loading &&
                        <FlatList
                            refreshControl={this.refreshControl()}
                            data={this.props.tournaments || []}
                            renderItem={this.renderItem}
                            ItemSeparatorComponent={() => <Divider />}
                            ListEmptyComponent={() => this.renderEmpty()}
                            getItemLayout={this.getItemLayout}
                            initialNumToRender={30}
                            maxToRenderPerBatch={30}
                            windowSize={30}
                            keyExtractor={item =>
                                `${item.tournament.uniqueId}-${item.tournament.id}`}
                        />
                    }
                </CompetitionListWraper>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        paddingBottom: 4,
        paddingTop: 8,
        // borderBottomWidth: 1,
        marginHorizontal: 8,
    }
};

const HomeScreenWithTheme = withDateUpdated(withTheme(HomeScreen));

HomeScreenWithTheme.navigationOptions = ({ navigation }) => ({
    title: 'Futboll',
    header: Header,
    headerLeft: (
        <ButtonIcon icon="reorder" flat onPress={() => navigation.openDrawer()} />
    ),
    headerRight: (
        <View style={{ width: 64, alignItems: 'center' }}>
        <ShowCalendar />
        </View>
    ),
});


export default HomeScreenWithTheme;
