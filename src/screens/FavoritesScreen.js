import React from 'react';
import { View, ScrollView, RefreshControl } from 'react-native';
import moment from 'moment';

import { withFavorites } from '../components/Global/FavProviderNew';
import CompetitionListWraper from '../components/Competition/CompetitionListWraper';
import EncountersItem from '../components/Competition/Encounters/EncountersItem';
import services from '../services';
import Divider from '../components/Global/Divider';
import { withTheme } from '../components/Global/Providers/ThemeProvider';
import ButtonIcon from '../components/Global/Button/ButtonIcon';
import Header from '../components/Global/Header/Header';
import IconInfo from '../components/Global/IconInfo';
import Theme from '../components/Global/Theme';
import Text from '../components/Global/Text';
import { Goal } from '../components/LiveScoreComp/MatchDetails/Events/Icons';


class FavoriteCompetitions extends React.Component {
    state = { };

    componentDidMount() {
      this.$isMounted = true;
      this.intervalID = setInterval(() => {
        this.props.refreshMatches();
      }, 40 * 1000);
    }

    componentWillUnmount() {
        this.$isMounted = false;
        clearInterval(this.intervalID);
    }
    
    onItemPress = (item) => {
        this.props.navigation.navigate('MatchDetails', { id: item.id });
    }

    onRefresh = () => {
        this.props.refreshMatches();
    }

    refreshControl() {
        return (
          <RefreshControl
            refreshing={this.props.refreshing}
            onRefresh={this.onRefresh} 
          />
        );
    }

    render() {
        const color = Theme.getTextColor(this.props.theme, 1);
        if (!this.props.matches.length) {
            return (
                <View 
                    style={{ 
                        flex: 1, 
                        backgroundColor: Theme.getBgColor(this.props.theme, 1),
                        padding: 10 
                    }} 
                >   
                    <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <Goal theme={this.props.theme} size={80} />
                    <Text style={{ color, lineHeight: 20 }}>
                        Ju nuk keni asnje ndeshje favorite
                    </Text>
                    </View>
                </View>
            );
        }

        const borderBottomColor = Theme.getTextColor(this.props.theme, 2);

        return (
            <ScrollView
                refreshControl={this.refreshControl()}
                style={{ 
                    flex: 1, 
                    backgroundColor: Theme.getBgColor(this.props.theme, 1) }}
            >
                <IconInfo
                    icon="star"
                    title="Ndeshjet e tua favorite"
                    iconProps={{ size: 22 }}
                    containerStyle={[styles.titleStyle, { borderBottomColor }]}
                    theme={this.props.theme}
                />
                <CompetitionListWraper>
                    {this.props.matches.map((item, index) => (
                        <View key={index}>
                            <EncountersItem
                                onPress={() => this.onItemPress(item)}
                                homeTeam={item.homeTeam.name}
                                homeTeamFlag={services.teamLogoUrl(item.homeTeam.id)}
                                awayTeam={item.awayTeam.name}
                                awayTeamFlag={services.teamLogoUrl(item.awayTeam.id)}
                                homeScore={item.homeScore.normaltime}
                                awayScore={item.awayScore.normaltime}
                                homeScoreCurrent={item.homeScore.current}
                                awayScoreCurrent={item.awayScore.current}
                                homeScoreP1={item.homeScore.period1}
                                awayScoreP1={item.awayScore.period1}
                                startTime={moment.unix(item.startTimestamp).local().format('HH:mm')}
                                status={item.status.type}
                                statusDescription={item.statusDescription}
                                matchId={item.id}
                                // isFavorite
                            />
                            {this.props.matches.length - 1 !== index ? <Divider /> : null}
                        </View>
                        )
                    )}
                </CompetitionListWraper>
            </ScrollView>

        );
    }
}

const styles = {
    titleStyle: {
        padding: 8,
        borderBottomWidth: 1,
        marginVertical: 4,
        marginHorizontal: 8
    }
};

const FavoritesWithTheme = withTheme(withFavorites(FavoriteCompetitions));

FavoritesWithTheme.navigationOptions = ({ navigation }) => {
    return {
        title: 'Favoritet',
        headerLeft: (
            <ButtonIcon icon="reorder" flat onPress={() => navigation.openDrawer()} />
        ),
        headerRight: (<View style={{ width: 64 }} />),
        header: Header
    };
};

export default FavoritesWithTheme;
