import React from 'react';
import {
    View,
    SectionList,
    ActivityIndicator,
    StyleSheet,
    RefreshControl,
    Image
} from 'react-native';
import moment from 'moment';

import { withTheme } from '../components/Global/Providers/ThemeProvider';
import services from '../services';
import Text from '../components/Global/Text';
import Encounters from '../components/Competition/Encounters/EncountersItem';
import Theme from '../components/Global/Theme';
import Header from '../components/Global/Header/Header';
import ButtonIcon from '../components/Global/Button/ButtonIcon';
import { withFavorites } from '../components/Global/FavProviderNew';
import Divider from '../components/Global/Divider';
import IconInfo from '../components/Global/IconInfo';
import { Goal } from '../components/LiveScoreComp/MatchDetails/Events/Icons';


class LiveScreen extends React.Component {
    state = {
        loading: false,
        liveMatches: [],
        refreshing: false
    }

    componentDidMount() {
        this.$isMounted = true;
        this.fetchData();
            this.intervalID = setInterval(() => {
                this.onRefresh();
            }, 40 * 1000);
    }

    componentWillUnmount() {
        this.$isMounted = false;
        clearInterval(this.intervalID);
    }

    onItemPress = (item) => {
        this.props.navigation.navigate('LiveMatchDetails', { id: item.id });
    }

    onRefresh = () => {
       this.setState({ refreshing: true });
       this.fetchData();
    }

    fetchData = () => {
        this.setState({ loading: true });
        services.getLiveMatches()
            .then(match => {
                if (this.$isMounted) {
                    this.setState({
                        liveMatches: match.sportItem.tournaments.map(item => {
                            const title = item;
                            const data = item.events;
                            return {
                                title,
                                data
                            };
                        }),
                        loading: false,
                        refreshing: false
                    });
                }
            })
            .catch(error => {
                console.log(error);
                this.setState({ loading: false, refreshing: false }); 
            });
    }

    refreshControl() {
        return (
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh} 
          />
        );
    }

    renderEmpty() {
        const green = Theme.getPrimaryColor(this.props.theme);
        const color = Theme.getTextColor(this.props.theme);

		if (this.state.loading) {
			return (
				<View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
					<ActivityIndicator size={40} color={green} />
					<Text style={{ color, fontWeight: 'bold' }} > loading </Text>
				</View>
			);
		}

		return (
            <View style={{ flex: 1, marginTop: 20 }}>   
            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
            <Goal theme={this.props.theme} size={60} />
            <Text style={{ color, lineHeight: 20 }}>
                Nuk ka ndeshje LIVE !
            </Text>
            </View>
        </View>
		);
	}

    renderItem = ({ item }) => {
        const {
            homeTeam,
            awayTeam,
            status,
            statusDescription,
            startTimestamp,
            homeScore,
            awayScore,
            id
        } = item;

        return (
            <Encounters
                homeTeam={homeTeam.name}
                homeTeamFlag={services.teamLogoUrl(homeTeam.id)}
                awayTeam={awayTeam.name}
                awayTeamFlag={services.teamLogoUrl(awayTeam.id)}
                homeScore={homeScore.normaltime}
                awayScore={awayScore.normaltime}
                homeScoreCurrent={homeScore.current}
                awayScoreCurrent={awayScore.current}
                homeScoreP1={homeScore.period1}
                awayScoreP1={awayScore.period1}
                startTime={moment.unix(startTimestamp).local().format('HH:mm')}
                status={status.type}
                statusDescription={statusDescription}
                onPress={() => this.onItemPress(item)}
                matchId={id}
            />
        );
    }

    renderSectionHeader = ({ section }) => {
        const { tournament, category } = section.title;
        const { theme } = this.props;
        const color = Theme.getTextColor(theme, 3);

        return (
            <View 
                style={[
                    styles.headerContainer, 
                    { 
                        borderBottomColor: color, 
                        backgroundColor: Theme.getBgColor(this.props.theme) 
                    }
                ]}
            >
                <IconInfo 
                    icon={
                        <Image
                            source={{ uri: services.tournamentLogoUrl(tournament.uniqueId) }}
                            style={{ width: 20, height: 20, resizeMode: 'contain' }}
                        />
                    }
                    title={`${category.name} / ${tournament.name}`}
                    titleProps={{ style: { fontSize: 14 } }}
                    theme={this.props.theme}
                />
            </View>
        );
    }

    render() {
        return (
            <SectionList
                stickySectionHeadersEnabled
                style={{
                    flex: 1,
                    backgroundColor: Theme.getBgColor(this.props.theme),
                }}
                refreshControl={this.refreshControl()}
                ItemSeparatorComponent={() => <Divider />}
                sections={this.state.liveMatches}
                renderSectionHeader={this.renderSectionHeader}
                renderItem={this.renderItem}
                keyExtractor={(item) => item.id.toString()}
                ListEmptyComponent={() => this.renderEmpty()}
                initialNumToRender={50}
                maxToRenderPerBatch={50}
                windowSize={50}
            />
        );
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        overflow: 'hidden',
        paddingHorizontal: 8,
        paddingVertical: 8,
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
});

const LiveScreenWithTheme = withTheme(withFavorites(LiveScreen));

LiveScreenWithTheme.navigationOptions = ({ navigation }) => ({
    title: 'Ndeshjet Live',
    header: Header,
    headerLeft: (
        <View>
            <ButtonIcon icon="reorder" flat onPress={() => navigation.openDrawer()} />
        </View>
    ),
    headerRight: (
        <View style={{ width: 64 }} />
    ),
});

export default LiveScreenWithTheme;
