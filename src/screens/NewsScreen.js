import React from 'react';
import { 
    View, 
    FlatList, 
    ActivityIndicator,
} from 'react-native';
import moment from 'moment';
import _ from 'lodash';

import ButtonIcon from '../components/Global/Button/ButtonIcon';
import { withTheme } from '../components/Global/Providers/ThemeProvider';
import Header from '../components/Global/Header/Header';
import CardItem from '../components/NewsComp/CardItem';
import ListItem from '../components/NewsComp/ListItem';
import Theme from '../components/Global/Theme';
import Text from '../components/Global/Text';
import services from '../services';
 

class NewsScreen extends React.PureComponent {
    state = { 
        cardLayout: true,
        news: [],
        loading: false,
        page: 1,
        refreshing: false
    };

    componentDidMount() {
        this.props.navigation.setParams({
            toggleLayout: this.toggleCard,
            cardLayout: this.state.cardLayout,
        });
        this.fetchData();
    }

    onItemPress = (item) => {
        this.props.navigation.navigate('OpenedNews', { data: item });
    }

    getImgUrl(_embedded) {
        return _.get(
            _embedded,
            'wp:featuredmedia[0].media_details.sizes.full.source_url',
            'https://dvynr1wh82531.cloudfront.net/sites/default/files/default_images/noImg_2.jpg'
        );
    }

    fetchData = () => {
        const { page } = this.state;
        this.setState({ loading: true });
        services.getNews(page)
            .then(res => {
                this.setState({ 
                    news: page === 1 ? res : [...this.state.news, ...res],
                    loading: false,
                    refreshing: false
                });
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    loading: false,
                    refreshing: false,
                });
            });
    }

    handleRefresh = () => {
        this.setState(
          {
            page: 1,
            refreshing: true,
          },
          () => {
            this.fetchData();
          }
        );
    };

    handleLoadMore = () => {
        this.setState(
          {
            page: this.state.page + 1
          },
          () => {
            this.fetchData();
          }
        );
    };
        
    toggleCard = () => {
        this.setState((prevState) => ({
            cardLayout: !prevState.cardLayout,
        }), () => {
            this.props.navigation.setParams({
                cardLayout: this.state.cardLayout,
            });
        });
    }

    renderFooter = () => {
        const green = Theme.getPrimaryColor(this.props.theme);
        const backgroundColor = Theme.getBgColor(this.props.theme, 2);

        if (!this.state.loading) return null;
    
        return (
          <View
            style={{
              overflow: 'hidden',
              marginVertical: 6,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text style={[styles.loadingStyle, { color: green, backgroundColor }]}>
                Loading...
            </Text>
          </View>
        );
      };

    renderItem = ({ item }) => {
        const { date, title, _embedded, subtitle } = item;
        return (
        <React.Fragment>
        {this.state.cardLayout ?
            <CardItem
                subtitle={subtitle}
                title={title.rendered}
                date={moment(date).format('DD-MM-YYYY')}
                imageUrl={this.getImgUrl(_embedded)}
                onPress={() => this.onItemPress(item)}
            />
            :
            <ListItem
                subtitle={subtitle}
                title={title.rendered}
                date={moment(date).format('DD-MM-YYYY')}
                imageUrl={this.getImgUrl(_embedded)}
                onPress={() => this.onItemPress(item)}
            />
        }
        </React.Fragment>
        );
    }

    renderEmpty() {
		if (this.state.loading) {
			return (
                <ActivityIndicator size={40} color="red" style={{ flex: 1 }} />
			);
		}

		return (
			<View style={{ flex: 1 }} >
				<Text style={{ fontWeight: 'bold', color: 'red' }} >LIST IS EMPTY</Text>
			</View>
		);
	}

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Theme.getBgColor(this.props.theme, 1) }}>
                <FlatList
                    data={this.state.news}
                    renderItem={this.renderItem}
                    extraData={this.state.cardLayout}
                    ListEmptyComponent={() => this.renderEmpty()}
                    keyExtractor={(item, index) => `${item.id}-${index}`}
                    ListFooterComponent={this.renderFooter}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refreshing}
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.5}
                />
            </View>
        );
    }
}

const styles = {
    loadingStyle: {
        textAlign: 'center', 
        borderWidth: 1, 
        borderRadius: 7, 
        padding: 6
    }
};

const NewsScreenWithTheme = withTheme(NewsScreen);

NewsScreenWithTheme.navigationOptions = ({ navigation }) => {
    const toggleLayout = navigation.getParam('toggleLayout', () => { });
    const isCardLayout = navigation.getParam('cardLayout', false);
    return {
        title: 'Lajmet',
        headerLeft: (
            <ButtonIcon icon="reorder" flat onPress={() => navigation.openDrawer()} />
        ),
        // headerRight: (<View style={{ width: 64 }} />),
        headerRight: (
            <View style={{ width: 64, alignItems: 'center' }}>
            <ButtonIcon 
                icon={isCardLayout ? 'view-list' : 'view-stream'} 
                flat onPress={toggleLayout} 
            />
            </View>
        ),
        header: Header
    };
};

export default NewsScreenWithTheme;
