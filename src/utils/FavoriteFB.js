import firebase from 'react-native-firebase';

function saveMatch(matchId) {
    firebase.messaging().subscribeToTopic(`event-${matchId}`);
    return firebase.firestore()
        .doc(`users/${firebase.auth().currentUser.uid}/matchFavorites/${matchId}`).set({
            id: matchId,
            createdAt: new Date(),
        }, { merge: true });
}


function removeMatch(matchId) {
    firebase.messaging().unsubscribeFromTopic(`event-${matchId}`);
    return firebase.firestore()
        .doc(`users/${firebase.auth().currentUser.uid}/matchFavorites/${matchId}`).delete();
}


function getMatch() {
    return new Promise(resolve => {
        firebase.firestore()
        .doc(`users/${firebase.auth().currentUser.uid}`).collection('matchFavorites')
        .get()
        .then(querySnapshot => {
            const matches = {};
            querySnapshot.forEach(match => {
                matches[match.id] = true;
            });
            resolve(matches);
        });
    });
}

export default {
    removeMatch,
    saveMatch,
    getMatch
};
