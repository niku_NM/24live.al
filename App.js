import React from 'react';
// import SplashScreen from 'react-native-splash-screen';
import firebase from 'react-native-firebase';

import Drawer from './src/Navigators/AllNav';
import ThemeProvider from './src/components/Global/Providers/ThemeProvider';
import CalendarProvider from './src/components/Global/Providers/CalendarProvider';
import FavoriteProviderNew from './src/components/Global/FavProviderNew';
import firebaseService from './src/services/firebase.service';
import NavigationService from './src/services/NavigationService';
import NotificationService from './src/services/notifications/NotificationControll';
// import FavoriteFB from './src/utils/FavoriteFB';

class App extends React.Component {
  state = { loading: false };

  componentDidMount() {
    this.notificationService = new NotificationService();
    this.notificationService.subscribe();
    // SplashScreen.hide();
    firebase.auth().signInAnonymouslyAndRetrieveData()
      .then(() => {
        firebaseService.registerNotificationToken(firebase.auth().currentUser.uid, true);
      })
      .catch(error => {
        console.log(error);
    });
  }

  componentWillUnmount() {
    this.notificationService.unsubcribe();
  }

  render() {
    return (
      <ThemeProvider theme="dark">
        <FavoriteProviderNew>
          <CalendarProvider>
            <Drawer 
                ref={(navigatorRef) => {
                  NavigationService.setTopLevelNavigator(navigatorRef);
                }}
            />
          </CalendarProvider>
        </FavoriteProviderNew>
      </ThemeProvider>
    );
  }
}

export default App;
